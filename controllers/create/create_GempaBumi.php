<?php 
	include "../../class/Gempa_Bumi.php";
	$gempa = new Gempa_Bumi();

	//id otomatis
	$kd = date('y')."3";
	$id=0;
	foreach ($gempa->getData() as $data ) {
	  if (((int)(substr($data['id_gempabumi'], 3)))>= $id) {
	    $id = ((int)(substr($data['id_gempabumi'], 3)));
	  }
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$gempa->id_gempabumi = $id_otomatis;
	$gempa->nama_lengkap = $_POST['nama_lengkap'];
	$gempa->kecamatan = $_POST['kecamatan'];
	$gempa->kelurahan = $_POST['kelurahan'];
	$gempa->dusun = $_POST['dusun'];
	$gempa->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$gempa->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$gempa->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$gempa->taksiran_kerugian = $_POST['taksiran_kerugian'];
	
	$gempa->skpd = "Belum";
	$gempa->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$gempa->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$gempa->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$gempa->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$gempa->kerusakan = "Berat";
	}
	
	//menampung hasil dari method crate 
	$error = $gempa->create_gempaBumi();

	
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}

	
?>
