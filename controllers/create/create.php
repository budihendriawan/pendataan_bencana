<?php 
include "../../class/Banjir.php";
$banjir = new Banjir();
include "../../class/Gempa_Bumi.php";
$gempa = new Gempa_Bumi();
include "../../class/Kebakaran.php";
$kebakaran = new Kebakaran();
include "../../class/Longsor.php";
$longsor = new Longsor();
include "../../class/Puting_beliung.php";
$puting_beliung = new Puting_Beliung();

// <option value="2">Longsor</option>
// <option value="3">Gempa Bumi</option>
// <option value="4">Kebakaran</option>
// <option value="5">Banjir</option>
// <option value="6">Angin Puting Beliung</option>

$cek = $_POST['bencana_alam'];

if($cek == 2){
	$kd = date('y')."2";
	$id=0;
	foreach ($longsor->getData() as $data ) {
		if (((int)(substr($data['id_longsor'], 3)))>= $id) {
			$id = ((int)(substr($data['id_longsor'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$longsor->id_longsor = $id_otomatis;	
	$longsor->nama_lengkap = $_POST['nama_lengkap'];
	$longsor->umur = $_POST['umur'];
	$longsor->kecamatan = $_POST['kecamatan'];
	$longsor->kelurahan = $_POST['kelurahan'];
	$longsor->dusun = $_POST['dusun'];
	$longsor->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$longsor->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$longsor->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$longsor->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$longsor->sebab_kejadian = $_POST['sebab_kejadian'];
	$longsor->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$longsor->skpd = "Belum";
	$longsor->admin_penginput = $_POST['admin_penginput'];
	
	date_default_timezone_set('Asia/Jakarta');
	$longsor->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$longsor->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$longsor->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$longsor->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$longsor->gambar1 = $fotobaru1;
		move_uploaded_file($tmp1, $path1);

	}else{
		$longsor->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$longsor->gambar2 = $fotobaru2;
		move_uploaded_file($tmp2, $path2);

	}else{
		$longsor->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$longsor->gambar3 = $fotobaru3;
		move_uploaded_file($tmp3, $path3);

	}else{
		$longsor->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$longsor->gambar4 = $fotobaru4;
		move_uploaded_file($tmp4, $path4);

	}else{
		$longsor->gambar4 = "none";
	}


	$error = $longsor->create_longsor();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}else if($cek == 3){
	$kd = date('y')."3";
	$id=0;
	foreach ($gempa->getData() as $data ) {
		if (((int)(substr($data['id_gempabumi'], 3)))>= $id) {
			$id = ((int)(substr($data['id_gempabumi'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$gempa->id_gempabumi = $id_otomatis;
	$gempa->nama_lengkap = $_POST['nama_lengkap'];
	$gempa->umur = $_POST['umur'];
	$gempa->kecamatan = $_POST['kecamatan'];
	$gempa->kelurahan = $_POST['kelurahan'];
	$gempa->dusun = $_POST['dusun'];
	$gempa->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$gempa->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$gempa->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$gempa->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$gempa->sebab_kejadian = $_POST['sebab_kejadian'];
	$gempa->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$gempa->skpd = "Belum";
	$gempa->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$gempa->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$gempa->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$gempa->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$gempa->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$gempa->gambar1 = $fotobaru1;
		move_uploaded_file($tmp1, $path1);

	}else{
		$gempa->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$gempa->gambar2 = $fotobaru2;
		move_uploaded_file($tmp2, $path2);

	}else{
		$gempa->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$gempa->gambar3 = $fotobaru3;
		move_uploaded_file($tmp3, $path3);

	}else{
		$gempa->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$gempa->gambar4 = $fotobaru4;
		move_uploaded_file($tmp4, $path4);

	}else{
		$gempa->gambar4 = "none";
	}
	$error = $gempa->create_gempaBumi();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}else if($cek == 4){
	$kd = date('y')."4";
	$id=0;
	foreach ($kebakaran->getData() as $data ) {
		if (((int)(substr($data['id_kebakaran'], 3)))>= $id) {
			$id = ((int)(substr($data['id_kebakaran'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$kebakaran->id_kebakaran = $id_otomatis;
	$kebakaran->nama_lengkap = $_POST['nama_lengkap'];
	$kebakaran->umur = $_POST['umur'];
	$kebakaran->kecamatan = $_POST['kecamatan'];
	$kebakaran->kelurahan = $_POST['kelurahan'];
	$kebakaran->dusun = $_POST['dusun'];
	$kebakaran->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$kebakaran->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$kebakaran->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$kebakaran->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$kebakaran->sebab_kejadian = $_POST['sebab_kejadian'];
	$kebakaran->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$kebakaran->skpd = "Belum";
	$kebakaran->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$kebakarankebakaran->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$kebakaran->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$kebakaran->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$kebakaran->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$kebakaran->gambar1 = $fotobaru1;
		move_uploaded_file($tmp1, $path1);

	}else{
		$kebakaran->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$kebakaran->gambar2 = $fotobaru2;
		move_uploaded_file($tmp2, $path2);
		
	}else{
		$kebakaran->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$kebakaran->gambar3 = $fotobaru3;
		move_uploaded_file($tmp3, $path3);

	}else{
		$kebakaran->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$kebakaran->gambar4 = $fotobaru4;
		move_uploaded_file($tmp4, $path4);

	}else{
		$kebakaran->gambar4 = "none";
	}
	$error = $kebakaran->create_kebakaran();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}else if($cek == 5){
	$kd = date('y')."5";
	$id=0;
	foreach ($banjir->getData() as $data ) {
		if (((int)(substr($data['id_banjir'], 3)))>= $id) {
			$id = ((int)(substr($data['id_banjir'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$banjir->id_banjir = $id_otomatis;
	$banjir->nama_lengkap = $_POST['nama_lengkap'];
	$banjir->umur = $_POST['umur'];
	$banjir->kecamatan = $_POST['kecamatan'];
	$banjir->kelurahan = $_POST['kelurahan'];
	$banjir->dusun = $_POST['dusun'];
	$banjir->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$banjir->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$banjir->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$banjir->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$banjir->sebab_kejadian = $_POST['sebab_kejadian'];
	$banjir->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$banjir->skpd = "Belum";
	$banjir->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$banjir->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$banjir->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$banjir->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$banjir->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$banjir->gambar1 = $fotobaru1;
		move_uploaded_file($tmp1, $path1);

	}else{
		$banjir->gambar1 = "none";
	}
	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$banjir->gambar2 = $fotobaru2;
		move_uploaded_file($tmp2, $path2);

	}else{
		$banjir->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$banjir->gambar3 = $fotobaru3;
		move_uploaded_file($tmp3, $path3);

	}else{
		$banjir->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$banjir->gambar4 = $fotobaru4;
		move_uploaded_file($tmp4, $path4);

	}else{
		$banjir->gambar4 = "none";
	}

	$error = $banjir->create_banjir();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}else if($cek == 6){
	$kd = date('y')."6";
	$id=0;
	foreach ($puting_beliung->getData() as $data ) {
		if (((int)(substr($data['id_putingBeliung'], 3)))>= $id) {
			$id = ((int)(substr($data['id_putingBeliung'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$puting_beliung->id_putingBeliung = $id_otomatis;
	$puting_beliung->nama_lengkap = $_POST['nama_lengkap'];
	$puting_beliung->umur = $_POST['umur'];
	$puting_beliung->kecamatan = $_POST['kecamatan'];
	$puting_beliung->kelurahan = $_POST['kelurahan'];
	$puting_beliung->dusun = $_POST['dusun'];
	$puting_beliung->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$puting_beliung->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$puting_beliung->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$puting_beliung->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$puting_beliung->sebab_kejadian = $_POST['sebab_kejadian'];
	$puting_beliung->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$puting_beliung->skpd = "Belum";
	$puting_beliung->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$puting_beliung->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$puting_beliung->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$puting_beliung->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$puting_beliung->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$puting_beliung->gambar1 = $fotobaru1;
		move_uploaded_file($tmp1, $path1);

	}else{
		$puting_beliung->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$puting_beliung->gambar2 = $fotobaru2;
		move_uploaded_file($tmp2, $path2);

	}else{
		$puting_beliung->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$puting_beliung->gambar3 = $fotobaru3;
		move_uploaded_file($tmp3, $path3);

	}else{
		$puting_beliung->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$puting_beliung->gambar4 = $fotobaru4;
		move_uploaded_file($tmp4, $path4);

	}else{
		$puting_beliung->gambar4 = "none";
	}



	

	$error = $puting_beliung->create_putingBeliung();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}

die();
if($cek == 2){
	$kd = date('y')."2";
	$id=0;
	foreach ($longsor->getData() as $data ) {
		if (((int)(substr($data['id_longsor'], 3)))>= $id) {
			$id = ((int)(substr($data['id_longsor'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$longsor->id_longsor = $id_otomatis;	
	$longsor->nama_lengkap = $_POST['nama_lengkap'];
	$longsor->umur = $_POST['umur'];
	$longsor->kecamatan = $_POST['kecamatan'];
	$longsor->kelurahan = $_POST['kelurahan'];
	$longsor->dusun = $_POST['dusun'];
	$longsor->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$longsor->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$longsor->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$longsor->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$longsor->sebab_kejadian = $_POST['sebab_kejadian'];
	$longsor->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$longsor->skpd = "Belum";
	$longsor->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$longsor->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$longsor->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$longsor->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$longsor->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$longsor->gambar1 = $fotobaru1;
	}else{
		$longsor->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$longsor->gambar2 = $fotobaru2;
	}else{
		$longsor->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$longsor->gambar3 = $fotobaru3;
	}else{
		$longsor->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$longsor->gambar4 = $fotobaru4;
	}else{
		$longsor->gambar4 = "none";
	}

	move_uploaded_file($tmp1, $path1);
	move_uploaded_file($tmp2, $path2);
	move_uploaded_file($tmp3, $path3);
	move_uploaded_file($tmp4, $path4);

	$error = $longsor->create_longsor();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}

}if($cek == 3){
	$kd = date('y')."2";
	$id=0;
	foreach ($longsor->getData() as $data ) {
		if (((int)(substr($data['id_longsor'], 3)))>= $id) {
			$id = ((int)(substr($data['id_longsor'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$longsor->id_longsor = $id_otomatis;
	$longsor->nama_lengkap = $_POST['nama_lengkap'];
	$longsor->umur = $_POST['umur'];
	$longsor->kecamatan = $_POST['kecamatan'];
	$longsor->kelurahan = $_POST['kelurahan'];
	$longsor->dusun = $_POST['dusun'];
	$longsor->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$longsor->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$longsor->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$longsor->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$longsor->sebab_kejadian = $_POST['sebab_kejadian'];
	$longsor->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$longsor->skpd = "Belum";
	$longsor->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$longsor->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$longsor->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$longsor->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$longsor->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$longsor->gambar1 = $fotobaru1;
	}else{
		$longsor->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$longsor->gambar2 = $fotobaru2;
	}else{
		$longsor->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$longsor->gambar3 = $fotobaru3;
	}else{
		$longsor->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$longsor->gambar4 = $fotobaru4;
	}else{
		$longsor->gambar4 = "none";
	}

	move_uploaded_file($tmp1, $path1);
	move_uploaded_file($tmp2, $path2);
	move_uploaded_file($tmp3, $path3);
	move_uploaded_file($tmp4, $path4);

	$error = $longsor->create_longsor();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}if($cek == 4){
	$kd = date('y')."2";
	$id=0;
	foreach ($longsor->getData() as $data ) {
		if (((int)(substr($data['id_longsor'], 3)))>= $id) {
			$id = ((int)(substr($data['id_longsor'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$longsor->id_longsor = $id_otomatis;
	$longsor->nama_lengkap = $_POST['nama_lengkap'];
	$longsor->umur = $_POST['umur'];
	$longsor->kecamatan = $_POST['kecamatan'];
	$longsor->kelurahan = $_POST['kelurahan'];
	$longsor->dusun = $_POST['dusun'];
	$longsor->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$longsor->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$longsor->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$longsor->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$longsor->sebab_kejadian = $_POST['sebab_kejadian'];
	$longsor->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$longsor->skpd = "Belum";
	$longsor->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$longsor->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$longsor->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$longsor->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$longsor->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$longsor->gambar1 = $fotobaru1;
	}else{
		$longsor->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$longsor->gambar2 = $fotobaru2;
	}else{
		$longsor->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$longsor->gambar3 = $fotobaru3;
	}else{
		$longsor->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$longsor->gambar4 = $fotobaru4;
	}else{
		$longsor->gambar4 = "none";
	}

	move_uploaded_file($tmp1, $path1);
	move_uploaded_file($tmp2, $path2);
	move_uploaded_file($tmp3, $path3);
	move_uploaded_file($tmp4, $path4);

	$error = $longsor->create_longsor();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}if($cek == 5){

	$kd = date('y')."5";
	$id=0;
	foreach ($banjir->getData() as $data ) {
		if (((int)(substr($data['id_putingBeliung'], 3)))>= $id) {
			$id = ((int)(substr($data['id_putingBeliung'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$banjir->id_putingBeliung = $id_otomatis;
	$banjir->nama_lengkap = $_POST['nama_lengkap'];
	$banjir->umur = $_POST['umur'];
	$banjir->kecamatan = $_POST['kecamatan'];
	$banjir->kelurahan = $_POST['kelurahan'];
	$banjir->dusun = $_POST['dusun'];
	$banjir->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$banjir->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$banjir->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$banjir->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$banjir->sebab_kejadian = $_POST['sebab_kejadian'];
	$banjir->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$banjir->skpd = "Belum";
	$banjir->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$banjir->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$banjir->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$banjir->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$banjir->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$banjir->gambar1 = $fotobaru1;
	}else{
		$banjir->gambar1 = "none";
	}
	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$banjir->gambar2 = $fotobaru2;
	}else{
		$banjir->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$banjir->gambar3 = $fotobaru3;
	}else{
		$banjir->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$banjir->gambar4 = $fotobaru4;
	}else{
		$banjir->gambar4 = "none";
	}


	// print($puting_beliung->gambar1);
	// echo "<br>";
	// print($puting_beliung->gambar2);
	// echo "<br>";
	// print($puting_beliung->gambar3);
	// echo "<br>";
	// print($puting_beliung->gambar4);
	// echo "<br>";
	// die();


	move_uploaded_file($tmp1, $path1);
	move_uploaded_file($tmp2, $path2);
	move_uploaded_file($tmp3, $path3);
	move_uploaded_file($tmp4, $path4);


	$error = $banjir->create_banjir();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}if($cek == 6){

	$kd = date('y')."6";
	$id=0;
	foreach ($puting_beliung->getData() as $data ) {
		if (((int)(substr($data['id_putingBeliung'], 3)))>= $id) {
			$id = ((int)(substr($data['id_putingBeliung'], 3)));
		}
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$puting_beliung->id_putingBeliung = $id_otomatis;
	$puting_beliung->nama_lengkap = $_POST['nama_lengkap'];
	$puting_beliung->umur = $_POST['umur'];
	$puting_beliung->kecamatan = $_POST['kecamatan'];
	$puting_beliung->kelurahan = $_POST['kelurahan'];
	$puting_beliung->dusun = $_POST['dusun'];
	$puting_beliung->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$puting_beliung->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$puting_beliung->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$puting_beliung->taksiran_kerugian = $_POST['taksiran_kerugian'];
	$puting_beliung->sebab_kejadian = $_POST['sebab_kejadian'];
	$puting_beliung->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
	$puting_beliung->skpd = "Belum";
	$puting_beliung->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$puting_beliung->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$puting_beliung->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$puting_beliung->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$puting_beliung->kerusakan = "Berat";
	}

	$foto1 = $_FILES['gambar1']['name'];
	$foto2 = $_FILES['gambar2']['name'];
	$foto3 = $_FILES['gambar3']['name'];
	$foto4 = $_FILES['gambar4']['name']; 


	if(!empty($foto1)){
		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$id_otomatis."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$puting_beliung->gambar1 = $fotobaru1;
	}else{
		$puting_beliung->gambar1 = "none";
	}

	if(!empty($foto2)){
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$id_otomatis."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$puting_beliung->gambar2 = $fotobaru2;
	}else{
		$puting_beliung->gambar2 = "none";
	}

	if(!empty($foto3)){
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$id_otomatis."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$puting_beliung->gambar3 = $fotobaru3;
	}else{
		$puting_beliung->gambar3 = "none";
	}

	if(!empty($foto4)){
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$id_otomatis."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$puting_beliung->gambar4 = $fotobaru4;
	}else{
		$puting_beliung->gambar4 = "none";
	}


	// print($puting_beliung->gambar1);
	// echo "<br>";
	// print($puting_beliung->gambar2);
	// echo "<br>";
	// print($puting_beliung->gambar3);
	// echo "<br>";
	// print($puting_beliung->gambar4);
	// echo "<br>";
	// die();


	move_uploaded_file($tmp1, $path1);
	move_uploaded_file($tmp2, $path2);
	move_uploaded_file($tmp3, $path3);
	move_uploaded_file($tmp4, $path4);

	
	$error = $puting_beliung->create_putingBeliung();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
			//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
			//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}
}


