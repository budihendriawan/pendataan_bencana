<?php 
	include "../../class/Longsor.php";
	$longsor = new Longsor();

	//id otomatis
	$kd = date('y')."2";
	$id=0;
	foreach ($longsor->getData() as $data ) {
	  if (((int)(substr($data['id_longsor'], 3)))>= $id) {
	    $id = ((int)(substr($data['id_longsor'], 3)));
	  }
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$longsor->id_longsor = $id_otomatis;
	$longsor->nama_lengkap = $_POST['nama_lengkap'];
	$longsor->kecamatan = $_POST['kecamatan'];
	$longsor->kelurahan = $_POST['kelurahan'];
	$longsor->dusun = $_POST['dusun'];
	$longsor->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$longsor->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$longsor->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$longsor->taksiran_kerugian = $_POST['taksiran_kerugian'];
	
	$longsor->skpd = "Belum";
	$longsor->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$longsor->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$longsor->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$longsor->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$longsor->kerusakan = "Berat";
	}
	
	//menampung hasil dari method crate 
	$error = $longsor->create_longsor();
	
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../../index.php?page=form_input"); 
	}

	
?>
