<?php 
	include "../../class/Kebakaran.php";
	$kebakaran = new Kebakaran();

	//id otomatis
	$kd = date('y')."4";
	$id=0;
	foreach ($kebakaran->getData() as $data ) {
	  if (((int)(substr($data['id_kebakaran'], 3)))>= $id) {
	    $id = ((int)(substr($data['id_kebakaran'], 3)));
	  }
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$kebakaran->id_kebakaran = $id_otomatis;
	
	$kebakaran->nama_lengkap = $_POST['nama_lengkap'];
	$kebakaran->kecamatan = $_POST['kecamatan'];
	$kebakaran->kelurahan = $_POST['kelurahan'];
	$kebakaran->dusun = $_POST['dusun'];
	$kebakaran->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$kebakaran->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$kebakaran->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$kebakaran->taksiran_kerugian = $_POST['taksiran_kerugian'];
	
	$kebakaran->skpd = "Belum";
	$kebakaran->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$kebakaran->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$kebakaran->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$kebakaran->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$kebakaran->kerusakan = "Berat";
	}
	
	//menampung hasil dari method crate 
	$error = $kebakaran->create_kebakaran();

	
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}

	
?>
