<?php 
	include "../../class/Banjir.php";
	$banjir = new Banjir();

	//id otomatis
	$kd = date('y')."5";
	$id=0;
	foreach ($banjir->getData() as $data ) {
	  if (((int)(substr($data['id_banjir'], 3)))>= $id) {
	    $id = ((int)(substr($data['id_banjir'], 3)));
	  }
	}
	$id++;
	$id_otomatis = $kd."".$id;


	$banjir->id_banjir = $id_otomatis;
	$banjir->nama_lengkap = $_POST['nama_lengkap'];
	$banjir->kecamatan = $_POST['kecamatan'];
	$banjir->kelurahan = $_POST['kelurahan'];
	$banjir->dusun = $_POST['dusun'];
	$banjir->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$banjir->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$banjir->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$banjir->taksiran_kerugian = $_POST['taksiran_kerugian'];
	
	$banjir->skpd = "Belum";
	$banjir->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$banjir->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$banjir->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$banjir->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$banjir->kerusakan = "Berat";
	}
	
	//menampung hasil dari method crate 
	$error = $banjir->create_banjir();

	
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		session_start();
		$_SESSION['berhasil'] = "Berhasil Mengisi Ke database";
		header("location: ../../index.php?page=form_input"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=form_input"); 
	}

	
?>
