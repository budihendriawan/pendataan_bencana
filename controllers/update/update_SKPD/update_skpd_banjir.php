<?php 
include "../../../class/Banjir.php";
	$banjir = new Banjir();
	
	//mengisi attribute dengan hasil imputan 
	$banjir->id_banjir = $_POST['id_banjir'];
	$banjir->skpd = $_POST['skpd'];
	
	
	//menampung hasil dari method update
	$error = $banjir->update_skpd_banjir();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../../index.php?page=tabel_banjir"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan update kembali
		header("location: ../../../index.php?page=tabel_banjir");
	}

?>