<?php 
include "../../../class/Longsor.php";
	$longsor = new Longsor();
	
	//mengisi attribute dengan hasil imputan 
	$longsor->id_longsor = $_POST['id_longsor'];
	$longsor->skpd = $_POST['skpd'];
	
	
	//menampung hasil dari method update
	$error = $longsor->update_skpd_longsor();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../../index.php?page=tabel_longsor"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan update kembali
		header("location: ../../../index.php?page=tabel_longsor");
	}

?>