<?php 
include "../../../class/Gempa_Bumi.php";
	$gempaBumi = new Gempa_Bumi();
	
	//mengisi attribute dengan hasil imputan 
	$gempaBumi->id_gempabumi = $_POST['id_gempabumi'];
	$gempaBumi->skpd = $_POST['skpd'];
	
	
	//menampung hasil dari method update
	$error = $gempaBumi->update_skpd_gempaBumi();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../../index.php?page=tabel_gempaBumi"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan update kembali
		header("location: ../../../index.php?page=tabel_gempaBumi");
	}

?>