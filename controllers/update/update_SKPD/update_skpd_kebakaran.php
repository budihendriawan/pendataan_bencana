<?php 
include "../../../class/Kebakaran.php";
	$kebakaran = new Kebakaran();
	
	//mengisi attribute dengan hasil imputan 
	$kebakaran->id_kebakaran = $_POST['id_kebakaran'];
	$kebakaran->skpd = $_POST['skpd'];
	
	
	//menampung hasil dari method update
	$error = $kebakaran->update_skpd_kebakaran();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../../index.php?page=tabel_kebakaran"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan update kembali
		header("location: ../../../index.php?page=tabel_kebakaran");
	}

?>