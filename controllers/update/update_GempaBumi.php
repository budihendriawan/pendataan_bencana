<?php 
	include "../../class/Gempa_Bumi.php";
	$gempaBumi = new Gempa_Bumi();

	//id otomatis
	$gempaBumi->id_gempabumi = $_POST['id_gempabumi'];
	$gempaBumi->nama_lengkap = $_POST['nama_lengkap'];
	$gempaBumi->kecamatan = $_POST['kecamatan'];
	$gempaBumi->kelurahan = $_POST['kelurahan'];
	$gempaBumi->dusun = $_POST['dusun'];
	$gempaBumi->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$gempaBumi->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$gempaBumi->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$gempaBumi->taksiran_kerugian = $_POST['taksiran_kerugian'];
	
	// $gempaBumi->skpd = "Belum";
	$gempaBumi->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$gempaBumi->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$gempaBumi->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$gempaBumi->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$gempaBumi->kerusakan = "Berat";
	}
	




	//menampung hasil dari method crate 
	$error = $gempaBumi->update_gempaBumi();

	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		
		header("location: ../../index.php?page=tabel_gempaBumi"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../../index.php?page=update_gempaBumi&id_gempaBumi={$gempaBumi->id_gempabumi}"); 
	}

	
?>
