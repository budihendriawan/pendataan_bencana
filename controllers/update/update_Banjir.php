<?php 
	include "../../class/Banjir.php";
	$banjir = new Banjir();

	//id otomatis
	$banjir->id_banjir = $_POST['id_banjir'];
	$banjir->nama_lengkap = $_POST['nama_lengkap'];
	$banjir->kecamatan = $_POST['kecamatan'];
	$banjir->kelurahan = $_POST['kelurahan'];
	$banjir->dusun = $_POST['dusun'];
	$banjir->jumlah_jiwa = $_POST['jumlah_jiwa'];
	$banjir->tanggal_terjadi = $_POST['tanggal_kejadian'];
	$banjir->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
	$banjir->taksiran_kerugian = $_POST['taksiran_kerugian'];
	
	// $banjir->skpd = "Belum";
	$banjir->admin_penginput = $_POST['admin_penginput'];

	date_default_timezone_set('Asia/Jakarta');
	$banjir->tanggal_input = date('Y-m-d');

	if( $_POST['taksiran_kerugian'] <= 20000000){
		$banjir->kerusakan = "Ringan";
	}
	else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
		$banjir->kerusakan = "Sedang";
	}else if($_POST['taksiran_kerugian'] > 60000000){
		$banjir->kerusakan = "Berat";
	}
	
	//menampung hasil dari method crate 
	$error = $banjir->update_banjir();

	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){

		header("location: ../../index.php?page=tabel_banjir"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../../index.php?page=update_banjir&id_banjir={$banjir->id_banjir}"); 
	}

	
?>
