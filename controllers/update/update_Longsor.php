<?php 
include "../../class/Longsor.php";
$longsor = new Longsor();
$data_longsor = $longsor->getDetail($_POST['id_longsor']);

//id otomatis
$longsor->id_longsor = $_POST['id_longsor'];	
$longsor->nama_lengkap = $_POST['nama_lengkap'];
$longsor->umur = $_POST['umur'];
$longsor->kecamatan = $_POST['kecamatan'];
$longsor->kelurahan = $_POST['kelurahan'];
$longsor->dusun = $_POST['dusun'];
$longsor->jumlah_jiwa = $_POST['jumlah_jiwa'];
$longsor->tanggal_terjadi = $_POST['tanggal_kejadian'];
$longsor->tahun_terjadi = substr($_POST['tanggal_kejadian'],0,4);
$longsor->taksiran_kerugian = $_POST['taksiran_kerugian'];
$longsor->sebab_kejadian = $_POST['sebab_kejadian'];
$longsor->tindakan_dilakukan = $_POST['tindakan_dilakukan'];
$longsor->skpd = "Belum";
$longsor->admin_penginput = $_POST['admin_penginput'];

date_default_timezone_set('Asia/Jakarta');
$longsor->tanggal_input = date('Y-m-d');

if( $_POST['taksiran_kerugian'] <= 20000000){
	$longsor->kerusakan = "Ringan";
}
else if( $_POST['taksiran_kerugian'] > 20000000 && $_POST['taksiran_kerugian'] <= 60000000){
	$longsor->kerusakan = "Sedang";
}else if($_POST['taksiran_kerugian'] > 60000000){
	$longsor->kerusakan = "Berat";
}

$foto1 = $_FILES['gambar1']['name'];
$foto2 = $_FILES['gambar2']['name'];
$foto3 = $_FILES['gambar3']['name'];
$foto4 = $_FILES['gambar4']['name']; 


if(!empty($foto1)){
		//menghapus gambar yang ada di dalam folder dan database
	$hapus= $longsor->hapus_gambar($_POST['id_longsor']);

    // nama field gambar
	$lokasi1 = $hapus['gambar1'];
    // alamat tempat foto
	$hapus_gambar1 ="../../res/upload_gambar/$lokasi1";

    // script untuk menghapus gambar dari folder
	unlink($hapus_gambar1);

		$foto1 = $_FILES['gambar1']['name'];//nama gambarnya
		$tmp1 = $_FILES['gambar1']['tmp_name'];

		$fotobaru1 = "1"."_".$_POST['id_longsor']."_".date('dmYHis')."_".$foto1;//nama foto setelah di rename

		$path1 = "../../res/upload_gambar/".$fotobaru1;//di pindah directory ke sini

		$longsor->gambar1 = $fotobaru1;
		move_uploaded_file($tmp1, $path1);

	}else{
		$longsor->gambar1 = $data_longsor['gambar1'];
	}

	if(!empty($foto2)){
		$hapus= $longsor->hapus_gambar($_POST['id_longsor']);

    // nama field gambar
		$lokasi2 = $hapus['gambar2'];
    // alamat tempat foto
		$hapus_gambar2 ="../../res/upload_gambar/$lokasi2";
		unlink($hapus_gambar2);
		$foto2 = $_FILES['gambar2']['name'];//nama gambarnya
		$tmp2 = $_FILES['gambar2']['tmp_name'];

		$fotobaru2 = "2"."_".$_POST['id_longsor']."_".date('dmYHis')."_".$foto2;//nama foto setelah di rename

		$path2 = "../../res/upload_gambar/".$fotobaru2;//di pindah directory ke sini

		$longsor->gambar2 = $fotobaru2;
		move_uploaded_file($tmp2, $path2);

	}else{
		$longsor->gambar2 = $data_longsor['gambar2'];
	}

	if(!empty($foto3)){
		$hapus= $longsor->hapus_gambar($_POST['id_longsor']);

    // nama field gambar
		$lokasi3 = $hapus['gambar3'];
    // alamat tempat foto
		$hapus_gambar3 ="../../res/upload_gambar/$lokasi3";
		unlink($hapus_gambar3);
		$foto3 = $_FILES['gambar3']['name'];//nama gambarnya
		$tmp3 = $_FILES['gambar3']['tmp_name'];

		$fotobaru3 = "3"."_".$_POST['id_longsor']."_".date('dmYHis')."_".$foto3;//nama foto setelah di rename

		$path3 = "../../res/upload_gambar/".$fotobaru3;//di pindah directory ke sini

		$longsor->gambar3 = $fotobaru3;
		move_uploaded_file($tmp3, $path3);

	}else{
		$longsor->gambar3 = $data_longsor['gambar3'];
		
	}

	if(!empty($foto4)){
		$hapus= $longsor->hapus_gambar($_POST['id_longsor']);

    // nama field gambar
		$lokasi4s = $hapus['gambar4'];
    // alamat tempat foto
		$hapus_gambar4 ="../../res/upload_gambar/$lokasi4";
		unlink($hapus_gambar4);
		$foto4 = $_FILES['gambar4']['name'];//nama gambarnya
		$tmp4 = $_FILES['gambar4']['tmp_name'];

		$fotobaru4 = "4"."_".$_POST['id_longsor']."_".date('dmYHis')."_".$foto4;//nama foto setelah di rename

		$path4 = "../../res/upload_gambar/".$fotobaru4;//di pindah directory ke sini

		$longsor->gambar4 = $fotobaru4;
		move_uploaded_file($tmp4, $path4);

	}else{
		$longsor->gambar4 = $data_longsor['gambar4'];
	}


	echo $longsor->gambar1;
	echo "<br>";
	echo $longsor->gambar2;
	echo "<br>";
	echo $longsor->gambar3;
	echo "<br>";
	echo $longsor->gambar4;

	$error = $longsor->update_longsor();

		//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){

		header("location: ../../index.php?page=tabel_longsor"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama gagal
		session_start();
		$_SESSION['gagal'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../../index.php?page=update_longsor&id_longsor={$longsor->id_longsor}"); 
	}

	?>
