<?php
include "../../class/Kebakaran.php";

$kebakaran = new Kebakaran();
$tanggal_terjadi = $_GET['tanggalnya'];
$test = $kebakaran->getLaporanPertanggal($tanggal_terjadi);

$yangcocok = $test->num_rows;
if($yangcocok == 0){
	session_start();
	$_SESSION['gagal_print'] = "Data tidak di temukan";
	header("location: ../../index.php?page=tabel_kebakaran");
}else{
require('morepagestable.php');



$pdf = new PDF('P','pt');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image('../../res/img/lambang_kabupaten.png',29,38,80);

$pdf->SetFont('Times','B',10);
// $pdf->Image('../../res/img/lambang_kabupaten.png',30,10,40);
$pdf->Cell(10,0," ",0,1,"R");
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(0,5,"",0,1,"C");
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(0,5,"DAFTAR PERUBAHAN RAKPITULASI USULAN PENGAJUAN PASCA BENCANA",0,1,"C");
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(0,5,"SOSIAL KEBAKARAN DI WILAYAH KAPBUPATEN BANDUNG MELALUI BANTUAN",0,1,"C");
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(0,5,"SOSIAL YANG DIRENCANAKAN PADA TAHUN",0,1,"C");
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(0,5,date('Y'),0,1,"C");
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(0,5,"",0,1,"C");
$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,5,"Semua Data : ",0,1,"L");
date_default_timezone_set('Asia/Jakarta');
$pdf->Cell(0,5,date('d-m-Y'),0,1,"R");
$pdf->Ln();
$pdf->Ln();

$pdf->tablewidths = array(75, 55, 55, 55, 55, 70, 55, 55, 60);

	
	$data1[] = array('Nama KK','Kecamatan','Kelurahan','Dusun','Jumlah Jiwa','Tanggal Terjadi','Tingkat','Taksiran', 'SKPD');
	$pdf->SetFont('Arial','',7);
	foreach($kebakaran->getLaporanPertanggal($tanggal_terjadi) as $row) {
		$data[] = array($row['nama_lengkap'], $row['kecamatan'], $row['kelurahan'], $row['dusun'], $row['jumlah_jiwa'], $row['tanggal_terjadi'], $row['kerusakan'], $row['taksiran_kerugian'], $row['skpd']);	
	}
$pdf->morepagestable($data1);
$pdf->morepagestable($data);
$pdf->Output();
}
?>
