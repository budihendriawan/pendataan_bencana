<?php 
if( !class_exists('database') ) {
	require('database.php');
}
class Longsor{

	public $id_longsor;
	public $nama_lengkap;
	public $umur;
	public $kecamatan;
	public $kelurahan;	
	public $dusun;
	public $jumlah_jiwa;
	public $tanggal_terjadi;
	public $tahun_terjadi;
	public $taksiran_kerugian;
	public $sebab_kejadian;
	public $tindakan_dilakukan;
	public $kerusakan;
	public $skpd;
	public $admin_penginput;
	public $tanggal_input;
	public $gambar1;
	public $gambar2;
	public $gambar3;
	public $gambar4;

	public function getData(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from longsor ";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}
	public function hapus_gambar($id_longsor){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from longsor where id_longsor = '{$id_longsor}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}


	public function getDetail($id_longsor){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from longsor where id_longsor = '{$id_longsor}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
	public function getLaporanPertanggal($tanggal_terjadi){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from longsor where tanggal_terjadi = '{$tanggal_terjadi}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}

	public function getDataBelum($skpd){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select * from longsor where skpd = '{$skpd}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}
	public function create_longsor(){
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();

			//query menyimpan data
		$sql = "insert into longsor (id_longsor,nama_lengkap,umur,kecamatan,kelurahan,dusun,jumlah_jiwa,tanggal_terjadi,tahun_terjadi,taksiran_kerugian,sebab_kejadian,tindakan_dilakukan,kerusakan,skpd,admin_penginput,tanggal_input,gambar1,gambar2,gambar3,gambar4) values ('{$this->id_longsor}','{$this->nama_lengkap}','{$this->umur}','{$this->kecamatan}','{$this->kelurahan}','{$this->dusun}','{$this->jumlah_jiwa}','{$this->tanggal_terjadi}','{$this->tahun_terjadi}','{$this->taksiran_kerugian}','{$this->sebab_kejadian}','{$this->tindakan_dilakukan}','{$this->kerusakan}','{$this->skpd}','{$this->admin_penginput}','{$this->tanggal_input}','{$this->gambar1}','{$this->gambar2}','{$this->gambar3}','{$this->gambar4}') ";
			//esekusi query di atas
		$data = $dbConnect->query($sql);

			//menampung error query simpan data
		$error = $dbConnect->error;
			//menutup koneksi
		$dbConnect = $db->close();
			//menampilkan nilai error
		return $error;
	}

	public function update_longsor(){
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();
			//query sql nya
		$sql = "UPDATE longsor SET nama_lengkap = '{$this->nama_lengkap}',umur = '{$this->umur}', kecamatan = '{$this->kecamatan}', kelurahan = '{$this->kelurahan}', dusun = '{$this->dusun}', jumlah_jiwa = '{$this->jumlah_jiwa}', tanggal_terjadi = '{$this->tanggal_terjadi}', tahun_terjadi = '{$this->tahun_terjadi}', taksiran_kerugian = '{$this->taksiran_kerugian}', kerusakan = '{$this->kerusakan}', admin_penginput = '{$this->admin_penginput}', tanggal_input = '{$this->tanggal_input}', gambar1 = '{$this->gambar1}', gambar2 = '{$this->gambar2}', gambar3 = '{$this->gambar3}', gambar4 = '{$this->gambar4}' where id_longsor = '{$this->id_longsor}'";
			//esekusi query di atas
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}

	public function update_skpd_longsor(){
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();
			//query sql nya
		$sql = "UPDATE longsor SET skpd = '{$this->skpd}' where id_longsor = '{$this->id_longsor}'";
			//esekusi query di atas
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}
	public function delete_longsor(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "delete from longsor where id_longsor = '{$this->id_longsor}'";
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}

	public function getJumlahData_longsor(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select count(id_longsor) from longsor";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
	public function getDataHasil_longsor(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "SELECT COUNT(id_longsor) AS Total_Longsor, tahun_terjadi AS Tahun_Longsor FROM longsor GROUP BY tahun_terjadi";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}




	



}

?>