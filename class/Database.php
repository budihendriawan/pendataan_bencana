<?php 
	
	class Database {
		
		public $conn;
		
		// method untuk menghubungkan ke database
		public function connect(){
			$this->conn = new mysqli("localhost", "root", "", "db_pendataan_bencana1");
			return $this->conn;
		}
		
		// method untuk memutuskan koneksi dengan database
		public function close(){
			return $this->conn->close();
		}
	}
	

?>