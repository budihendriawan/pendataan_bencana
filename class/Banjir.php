<?php 
if( !class_exists('database') ) {
	require('database.php');
}
class Banjir{

	public $id_banjir;
	public $nama_lengkap;
	public $umur;
	public $kecamatan;
	public $kelurahan;	
	public $dusun;
	public $jumlah_jiwa;
	public $tanggal_terjadi;
	public $tahun_terjadi;
	public $taksiran_kerugian;
	public $sebab_kejadian;
	public $tindakan_dilakukan;
	public $kerusakan;
	public $skpd;
	public $admin_penginput;
	public $tanggal_input;
	public $gambar1;
	public $gambar2;
	public $gambar3;
	public $gambar4;


	public function getData(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from banjir ";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}

	public function getDetail($id_banjir){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from banjir where id_banjir = '{$id_banjir}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
	public function getLaporanPertanggal($tanggal_terjadi){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from banjir where tanggal_terjadi = '{$tanggal_terjadi}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}

	public function create_banjir(){
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();
		
			//query menyimpan data
		$sql = "insert into banjir (id_banjir,nama_lengkap,umur,kecamatan,kelurahan,dusun,jumlah_jiwa,tanggal_terjadi,tahun_terjadi,taksiran_kerugian,sebab_kejadian,tindakan_dilakukan,kerusakan,skpd,admin_penginput,tanggal_input,gambar1,gambar2,gambar3,gambar4) values ('{$this->id_banjir}','{$this->nama_lengkap}','{$this->umur}','{$this->kecamatan}','{$this->kelurahan}','{$this->dusun}','{$this->jumlah_jiwa}','{$this->tanggal_terjadi}','{$this->tahun_terjadi}','{$this->taksiran_kerugian}','{$this->sebab_kejadian}','{$this->tindakan_dilakukan}','{$this->kerusakan}','{$this->skpd}','{$this->admin_penginput}','{$this->tanggal_input}','{$this->gambar1}','{$this->gambar2}','{$this->gambar3}','{$this->gambar4}')";
			//esekusi query di atas
		$data = $dbConnect->query($sql);
		
			//menampung error query simpan data
		$error = $dbConnect->error;
			//menutup koneksi
		$dbConnect = $db->close();
			//menampilkan nilai error
		return $error;
	}

	public function update_banjir(){
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();
			//query sql nya
		$sql = "UPDATE banjir SET nama_lengkap = '{$this->nama_lengkap}', kecamatan = '{$this->kecamatan}', kelurahan = '{$this->kelurahan}', dusun = '{$this->dusun}', jumlah_jiwa = '{$this->jumlah_jiwa}', tanggal_terjadi = '{$this->tanggal_terjadi}', tahun_terjadi = '{$this->tahun_terjadi}', taksiran_kerugian = '{$this->taksiran_kerugian}', kerusakan = '{$this->kerusakan}', admin_penginput = '{$this->admin_penginput}', tanggal_input = '{$this->tanggal_input}' where id_banjir = '{$this->id_banjir}'";
			//esekusi query di atas
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}

	public function update_skpd_banjir(){
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();
			//query sql nya
		$sql = "UPDATE banjir SET skpd = '{$this->skpd}' where id_banjir = '{$this->id_banjir}'";
			//esekusi query di atas
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}
	public function delete_banjir(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "delete from banjir where id_banjir = '{$this->id_banjir}'";
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}

	public function getJumlahData_banjir(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select count(id_banjir) from banjir";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
	public function getDataBelum($skpd){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select * from banjir where skpd = '{$skpd}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}



	public function getDataHasil_banjir(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "SELECT COUNT(banjir.id_banjir) AS Total_Banjir, banjir.tahun_terjadi AS Tahun FROM banjir GROUP BY banjir.tahun_terjadi";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}
	public function hapus_gambar($id_banjir){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from banjir where id_banjir = '{$id_banjir}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
	



}

?>