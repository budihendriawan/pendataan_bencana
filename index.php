<?php 

session_start();
if(!isset($_SESSION['berhasil_masuk'])){
    header('location: login.php');
}else{
   $nama = $_SESSION['berhasil_masuk']['nama_admin'];

}
?>
<!doctype html>
<html lang="en">

<head>
  <title>Pendataan</title>
   <link rel="icon" type="image/png" href="res/img/lambang_kabupaten.png">


  <!-- Data tables --> 

  <link rel="stylesheet" type="text/css" href="plugin/DataTables/datatables.min.css"> <!-- link css data tables -->
   <link rel="stylesheet" type="text/css" href="plugin/DataTables/DataTables-1.10.18/css/dataTables.jqueryui.min.css">
   <link rel="stylesheet" type="text/css" href="plugin/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js">
   <!-- <script type="text/javascript" src="assets/js/jquery.ajax.js"></script> -->

  <!-- Chart --> 
   <script type="text/javascript" src="plugin/Chart.js/Chart.bundle.js"></script>
   <script type="text/javascript" src="page/admin/ajax/ajax.js"></script>

  <!-- Material Kit CSS -->
  <link href="assets/css/material-dashboard.css" rel="stylesheet" />
  <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" /> -->

  <link href="assets/css/material_icon.css" rel="stylesheet" />
  <script type="text/javascript" src="plugin/Chart.js/Chart.bundle.js"></script> 




</head>

<body class="">
<?php 
      //cek halaman yang di tuju
      array_key_exists('page', $_GET) ? $page = $_GET['page'] : $page = '';
    ?>
    
  <div class="wrapper ">
    <div class="sidebar" data-color="azure" data-background-color="black" data-image="./assets/img/boboy.jpg">
    
      <div class="logo">
        <a href="" class="simple-text logo-normal">
        <center><img src="res/img/lambang_kabupaten.png" width="200" height="200"></center>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?php if($page=='' || $page=='dashboard') echo "active"; ?>  ">
            <a class="nav-link" href="index.php">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
           <li class="nav-item <?php if($page=='form_input') echo "active"; ?>">
            <a class="nav-link" href='index.php?page=form_input'>
              <i class="material-icons">comments</i>
              <p>Form Bencana</p>
            </a>
          </li>
           <li class="nav-item <?php if($page=='tabel') echo "active"; ?>">
            <a class="nav-link" href='index.php?page=tabel'>
              <i class="material-icons">home</i>
              <p>Master Bencana</p>
            </a>
          </li>
         <!--  <li class="nav-item ">
            <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="false">
              <i class="material-icons">image</i>
              <p> Pages
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="pagesExamples" style="">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/pricing.html">
                    <span class="sidebar-mini"> P </span>
                    <span class="sidebar-normal"> Pricing </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/rtl.html">
                    <span class="sidebar-mini"> RS </span>
                    <span class="sidebar-normal"> RTL Support </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/timeline.html">
                    <span class="sidebar-mini"> T </span>
                    <span class="sidebar-normal"> Timeline </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/login.html">
                    <span class="sidebar-mini"> LP </span>
                    <span class="sidebar-normal"> Login Page </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/register.html">
                    <span class="sidebar-mini"> RP </span>
                    <span class="sidebar-normal"> Register Page </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/lock.html">
                    <span class="sidebar-mini"> LSP </span>
                    <span class="sidebar-normal"> Lock Screen Page </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/user.html">
                    <span class="sidebar-mini"> UP </span>
                    <span class="sidebar-normal"> User Profile </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/pages/error.html">
                    <span class="sidebar-mini"> E </span>
                    <span class="sidebar-normal"> Error Page </span>
                  </a>
                </li>
              </ul>
            </div>
          </li> -->
          <!-- your sidebar here -->
        </ul>
      </div>
    </div>
    <div class="main-panel" style="background-color: ;">
      <!-- Navbar -->
     
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top" ><!-- -->
        <div class="container-fluid">
         <div class="navbar col-lg-12 bg-dark" style="padding: 10px 10px 10px 10px; ">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="">Dashboard</a>
          </div>
          <div class="collapse navbar-collapse justify-content-end" >
            <ul class="navbar-nav">
               <li class="nav-item dropdown">
                <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <span><?php print($nama); ?></span>
                  </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="index.php?page=ubah_admin">Ubah Admin</a>
                  <a class="dropdown-item" href="index.php?page=buat_admin">Buat Admin</a>
                  <a class="dropdown-item" href="" data-toggle="modal" data-target="#welcome">Keluar</a>
               
                </div>
              </li>

              <!-- your navbar here -->
            </ul>
          </div>
        </div>
          </div>
      </nav>
    
      <!-- End Navbar -->
        <div class="modal fade" style="padding-top: 3%;" id="welcome" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Anda Yakin Ingin Keluar?</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                  </button>
                </div>
                <div class="modal-footer">
                  <a href="controllers/admin/logout.php" class="btn btn-primary btn-xs">Yakin</a>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
                </div>
              </div>
            </div>
            </div>
    
      <div class="content" style="background-color: ;"><!-- style= background-color: blue; -->
        <div class="container-fluid">
        <?php 

         $path = "page/route.php";
         require_once $path;




        ?>
         
        </div>
        </div>
      

   

    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js"></script>
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  

  <!-- <script src="./assets/js/plugins/bootstrap-notify.js"></script> -->
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-dashboard.js"></script>
  <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->

 <script src="plugin/DataTables/dataTables.bootstrap.min.js" ></script>
 <!-- <script src="plugin/DataTables/jquery-3.1.0.js"></script> -->
 <script src="plugin/DataTables/jquery.dataTables.min.js"></script>
 <!-- <script src="plugin/DataTables/tabel.js"></script> -->
 <script type="text/javascript">
   $(document).ready(function() {
    $('#tabel_longsor').DataTable( {

"dom": '<"top"<"pull-left"f><"pull-right"l>>rt<"bottom"<"pull-left"i><"pull-right"p>>',

"language": {

"search": "Cari : ",

"lengthMenu": "Tampilkan _MENU_ baris",

"zeroRecords": "Maaf – Data tidak ada",

"info": "Halaman _PAGE_ dari _PAGES_",

"infoEmpty": "Tidak ada data",

"infoFiltered": "(pencarian dari _MAX_ data)"

},

responsive: true,

stateSave: true // keep paging

} );

    $('#tabel_gempaBumi').DataTable();
    $('#tabel_kebakaran').DataTable();
    $('#tabel_banjir').DataTable();
 
  } );
 </script>

  
 
  
</body>

</html>