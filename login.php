<?php 
  session_start();
  if(isset($_SESSION['berhasil_masuk'])){
    header("location: index.php");

  }
  

?>


<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
   <link rel="icon" type="image/png" href="res/img/lambang_kabupaten.png">
  
	  <!-- Material Kit CSS -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <link href="assets/css/material_icon.css" rel="stylesheet" />
</head>
<body>
<div class="section section-signup page-header" style="background-color: white; padding-top: 4%;">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-8 ml-auto mr-auto">
           <?php if(isset($_SESSION['gagal_masuk'])): ?>
                   <div class="alert alert-danger">
                  <div class="container">
                   <center><?php print($_SESSION['gagal_masuk']); ?></center>
                  </div>
                </div>
                      <?php unset($_SESSION['gagal_masuk']); ?>
            <?php endif; ?>
            <div class="card card-login">
              <form class="form" method="POST" action="controllers/admin/ceklogin.php">
                <div class="card-header card-header-info text-center">
                  <center><img src="res/img/lambang_kabupaten.png" width="200" height="200"></center>
                 
                
                </div>
                <p class="description text-center"></p>
                <div class="card-body">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off">
                  </div>
                  <br>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" class="form-control" placeholder="Password" name="password">
                  </div>
                  <br>
                </div>
                <div class="footer text-center" style="padding-left: 20%; padding-right: 20%;">
                  <button type="submit" class="btn btn-primary btn-md" >Sign In</button>&nbsp;
                  <button type="Reset" class="btn btn-primary btn-md" >Reset</button>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

</body>
</html>