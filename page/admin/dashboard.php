<?php 
require_once "class/Banjir.php";
$banjir = new Banjir();
require_once "class/Longsor.php";
$longsor = new Longsor();
require_once "class/Gempa_Bumi.php";
$gempa = new Gempa_Bumi();
require_once "class/Kebakaran.php";
$kebakaran = new Kebakaran();
require_once "class/Puting_beliung.php";
$puting_beliung = new Puting_Beliung();


$jumlah_banjir = $banjir->getJumlahData_banjir();
$jumlah_gempa = $gempa->getJumlahData_gempaBumi();
$jumlah_kebakaran = $kebakaran->getJumlahData_kebakaran();
$jumlah_longsor = $longsor->getJumlahData_longsor();
$jumlah_putingBeliung = $puting_beliung->getJumlahData_putingBeliung();

?> 
<div id="nav-tabs">
  <div class="row">
    <div class="col-md-7">
      <!-- Tabs with icons on Card -->
      <div class="card card-nav-tabs">
        <div class="card-header card-header-danger">
          <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
          <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
              <ul class="nav nav-tabs" data-tabs="tabs">
                <li class="nav-item">
                  <a class="nav-link active" href="#longsor" data-toggle="tab">
                    <i class="material-icons">insert_chart</i> Longsor
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#gempa_bumi" data-toggle="tab">
                    <i class="material-icons">insert_chart</i> Gempa Bumi
                  </a>
                </li><br>
                <li class="nav-item">
                  <a class="nav-link" href="#kebakaran" data-toggle="tab">
                    <i class="material-icons">insert_chart</i> Kebakaran	
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#banjir" data-toggle="tab">
                    <i class="material-icons">insert_chart</i> Banjir
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#putingBeliung" data-toggle="tab">
                    <i class="material-icons">insert_chart</i> Puting Beliung
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="card-body ">
          <div class="tab-content text-center">
            <div class="tab-pane active" id="longsor">
              <table class="table">
                <canvas id="myChart" width="200" height="200"></canvas>
                <script>
                 var ctx = document.getElementById("myChart").getContext('2d');
                 var myChart = new Chart(ctx, {
                  type: 'bar',
                  data: {
                   labels: [
                   <?php
                   foreach ($longsor->getDataHasil_longsor() as $data) {
                    print("'".$data['Tahun_Longsor']."'".",");
                  }
                  ?>
                  
                  ],
                  datasets: [{
                    label: 'jumlah data longsor ', 
                    data: [
                    <?php
                    foreach ($longsor->getDataHasil_longsor() as $data) {
                     print("'".$data['Total_Longsor']."'".",");
                   }
                   ?>
                   ],
                   backgroundColor: [
                   <?php 
                   for($i = 0; $i <= 100; $i++){
                     echo "'rgb(110, 220, 239, 0.5)',";
                   }
                   ?>        
                   ],
                   borderColor: [
                   <?php 
                   for($i = 0; $i <= 100; $i++){
                     echo "'rgba(75, 192, 192, 1)',";
                   }

                   ?>
                   ],
                   borderWidth: 1
                 }]
               },
               options: {
                 scales: {
                  yAxes: [{
                   ticks: {
                    beginAtZero:true
                  }
                }]
              }
            }
          });
        </script>
      </table>
    </div>
    <div class="tab-pane" id="gempa_bumi">
      <table class="table">
        <canvas id="myChart1" width="200" height="200"></canvas>
        <script>
         var ctx = document.getElementById("myChart1").getContext('2d');
         var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
           labels: [
           <?php
           foreach ($gempa->getDataHasil_gempaBumi() as $data) {
            print("'".$data['Tahun_gempaBumi']."'".",");
          }
          ?>
          
          ],
          datasets: [{
            label: 'jumlah data gempa bumi ', 
            data: [
            <?php
            foreach ($gempa->getDataHasil_gempaBumi() as $data) {
             print("'".$data['Total_gempaBumi']."'".",");
           }
           ?>
           ],
           backgroundColor: [
           <?php 
           for($i = 0; $i <= 100; $i++){
             echo "'rgb(110, 220, 239, 0.5)',";
           }
           ?>        
           ],
           borderColor: [
           <?php 
           for($i = 0; $i <= 100; $i++){
             echo "'rgba(75, 192, 192, 1)',";
           }

           ?>
           ],
           borderWidth: 1
         }]
       },
       options: {
         scales: {
          yAxes: [{
           ticks: {
            beginAtZero:true
          }
        }]
      }
    }
  });
</script>
</table>
</div>
<div class="tab-pane" id="kebakaran">
 <table class="table">
  <canvas id="myChart2" width="200" height="200"></canvas>
  <script>
   var ctx = document.getElementById("myChart2").getContext('2d');
   var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
     labels: [
     <?php
     foreach ($kebakaran->getDataHasil_kebakaran() as $data) {
      print("'".$data['Tahun_kebakaran']."'".",");
    }
    ?>

    ],
    datasets: [{
      label: 'jumlah data kebakaran ', 
      data: [
      <?php
      foreach ($kebakaran->getDataHasil_kebakaran() as $data) {
       print("'".$data['Total_kebakaran']."'".",");
     }
     ?>
     ],
     backgroundColor: [
     <?php 
     for($i = 0; $i <= 100; $i++){
       echo "'rgb(110, 220, 239, 0.5)',";
     }
     ?>        
     ],
     borderColor: [
     <?php 
     for($i = 0; $i <= 100; $i++){
       echo "'rgba(75, 192, 192, 1)',";
     }

     ?>
     ],
     borderWidth: 1
   }]
 },
 options: {
   scales: {
    yAxes: [{
     ticks: {
      beginAtZero:true
    }
  }]
}
}
});
</script>
</table>
</div>
<div class="tab-pane" id="banjir">
 <table class="table">
   <canvas id="myChart3" width="200" height="200"></canvas>
   <script>
    var ctx = document.getElementById("myChart3").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [
        <?php
        foreach ($banjir->getDataHasil_banjir() as $data) {
         print("'".$data['Tahun']."'".",");
       }
       ?>
       
       ],
       datasets: [{
        label: 'jumlah data Banjir ', 
        data: [
        <?php
        foreach ($banjir->getDataHasil_banjir() as $data) {
          print("'".$data['Total_Banjir']."'".",");
        }
        ?>
        ],
        backgroundColor: [
        <?php 
        for($i = 0; $i <= 100; $i++){
          echo "'rgb(110, 220, 239, 0.5)',";
        }
        ?>        
        ],
        borderColor: [
        <?php 
        for($i = 0; $i <= 100; $i++){
          echo "'rgba(75, 192, 192, 1)',";
        }

        ?>
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero:true
          }
        }]
      }
    }
  });
</script>
</table>
</div>
<div class="tab-pane" id="putingBeliung">
 <table class="table">
   <canvas id="myChart4" width="200" height="200"></canvas>
   <script>
    var ctx = document.getElementById("myChart4").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [
        <?php
        foreach ($puting_beliung->getDataHasil_putingBeliung() as $data) {
         print("'".$data['Tahun_PutingBeliung']."'".",");
       }
       ?>
       
       ],
       datasets: [{
        label: 'jumlah data Banjir ', 
        data: [
        <?php
        foreach ($puting_beliung->getDataHasil_putingBeliung() as $data) {
          print("'".$data['Total_PutingBeliung']."'".",");
        }
        ?>
        ],
        backgroundColor: [
        <?php 
        for($i = 0; $i <= 100; $i++){
          echo "'rgb(110, 220, 239, 0.5)',";
        }
        ?>        
        ],
        borderColor: [
        <?php 
        for($i = 0; $i <= 100; $i++){
          echo "'rgba(75, 192, 192, 1)',";
        }

        ?>
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero:true
          }
        }]
      }
    }
  });
</script>
</table>
</div>
</div>
</div>
</div>
<!-- End Tabs with icons on Card -->
</div>
<div class="col-md-5">
  <div class="card">
    <div class="card-header card-header-danger">
      <div class="row">
        <div class="col-md-12" style="">
         <h3 class="card-title"><center>Jumlah Bencana Keseluruhan</center></h3>
       </div>
     </div>
     
   </div>
   <div class="card-body">
    <div class="table-responsive">

     <canvas id="myChart11" width="200" height="200"></canvas>
     <script>
      var ctx = document.getElementById("myChart11").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Longsor" , "Gempa Bumi"  , "Kebakaran" , "Banjir", "Puting Beliung"],
          datasets: [{ 
            data: [
            <?= $jumlah_longsor['0']; ?> ,
            <?= $jumlah_gempa['0']; ?> ,
            <?= $jumlah_kebakaran['0']; ?> ,
            <?= $jumlah_banjir['0']; ?> ,
            <?= $jumlah_putingBeliung['0'] ?> 
            
            

            ],
            

            backgroundColor: [
            
            
            'rgb(191, 255, 0, 0.7)',
            'rgb(64, 255, 0, 0.7)',
            'rgb(21, 181, 151, 0.7)',
            'rgb(255, 0, 255, 0.7)'
            
            ],
            borderColor: [
            
            
            'rgb(64, 255, 0,1)',
            'rgb(0, 255, 0,1)',
            'rgb(21, 181, 151,1)',
            'rgb(191, 0, 255,1)'
            
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          }
        }
      });
    </script>
    <script type="text/javascript">
     $("#myChart11").click(function(){
      console.log('Hai');
    });

  </script>


</div>
</div>
</div>
</div>

</div>
</div>
<div class="ripple-container"></div></button><div class="dropdown-menu" role="combobox" style="overflow: hidden; position: absolute; top: -235px; left: 1px; will-change: top, left;" x-placement="top-start"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1" style="overflow-y: auto;"><ul class="dropdown-menu inner show"><li class="disabled"><a role="option" class="dropdown-item disabled" aria-disabled="true" tabindex="-1" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Single Option</span></a></li><li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Foobar</span></a></li><li class="selected active"><a role="option" class="dropdown-item selected active" aria-disabled="false" tabindex="0" aria-selected="true"><span class=" bs-ok-default check-mark"></span><span class="text">Is great</span><div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 47px; top: 3px; background-color: rgb(255, 255, 255); transform: scale(26.25);"></div></div></a></li><li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Is bum</span></a></li><li class=""><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Is wow</span><div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 78px; top: 36px; background-color: rgb(96, 96, 96); transform: scale(26.25);"></div></div></a></li><li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">boom</span></a></li></ul></div></div></div>
