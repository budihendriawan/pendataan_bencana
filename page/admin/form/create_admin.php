 <div class="content">
        <div class="container-fluid" >
        <div class="" style="padding-left: 25%;">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Buat Admin Baru</h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="">
                   <form action="controllers/create/create_Admin.php" method="POST">
                <div class="row">
                
        
                  
                    <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">person</i>
                          </span>
                        </div>
                        
                         <label for="nama_admin" class="bmd-label-floating" style="padding-left: 12%;">Nama Admin</label>
                         <input type="text" name="nama_admin" class="form-control" id="nama_admin" autocomplete="off" value="" required>
                      </div>
                      </div>
                  </div>
                   <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">face</i>
                          </span>
                        </div>
                         <label for="username" class="bmd-label-floating" style="padding-left: 12%;">Username Admin</label>
                         <input type="text" name="username" class="form-control" id="username" autocomplete="off" value="" required>
                      </div>
                      </div>
                  </div>
                   <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">lock</i>
                          </span>
                        </div>
                         <label for="password" class="bmd-label-floating" style="padding-left: 12%;">Password Admin</label>
                         <input type="password" name="password" class="form-control" id="password" autocomplete="off" value="" required>
                      </div>
                      </div>
                  </div>
                  
               
                
                  </div>
                  <div class="col-lg-12" align="right">
                  <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                  <button type="reset" class="btn btn-danger" >Reset</button>
                  <a href="index.php" class="btn btn-info" >Kembali</a>

                    
                  </div>
                  
                         
                </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
        </div>