<?php if($page=="delete_longsor") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Longsor : <?php print($_GET['id_longsor']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <form method="POST" action="controllers/delete/delete_longsor.php">
                    <input type="hidden" name="id_longsor" value="<?= $_GET['id_longsor'] ?>">
                    <!-- <input type="hidden" name="skpd" value="Sudah Di Bantu"> -->
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_longsor" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>
<?php if($page=="delete_gempaBumi") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Gema Bumi : <?php print($_GET['id_gempabumi']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <form method="POST" action="controllers/delete/delete_gempaBumi.php">
                    <input type="hidden" name="id_gempabumi" value="<?= $_GET['id_gempabumi'] ?>">
                    <input type="hidden" name="skpd" value="Sudah Di Bantu">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_gempaBumi" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>
<?php if($page=="delete_kebakaran") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Kebakaran : <?php print($_GET['id_kebakaran']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <form method="POST" action="controllers/delete/delete_kebakaran.php">
                    <input type="hidden" name="id_kebakaran" value="<?= $_GET['id_kebakaran'] ?>">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_kebakaran" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>

<?php if($page=="delete_banjir") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Banjir : <?php print($_GET['id_banjir']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <form method="POST" action="controllers/delete/delete_Banjir.php">
                    <input type="hidden" name="id_banjir" value="<?= $_GET['id_banjir'] ?>">
                    <!-- <input type="hidden" name="skpd" value="Sudah Di Bantu"> -->
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_banjir" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>
<?php if($page=="delete_putingBeliung") : ?>
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Puting Beliung : <?php print($_GET['id_putingBeliung']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <form method="POST" action="controllers/delete/delete_putingBeliung.php">
                    <input type="hidden" name="id_putingBeliung" value="<?= $_GET['id_putingBeliung'] ?>">
                    <!-- <input type="hidden" name="skpd" value="Sudah Di Bantu"> -->
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_banjir" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>

