<?php 
if(!isset($_SESSION)) 
{ 
  session_start(); 
} 

?>

<!-- mulai modals form -->
<div class="modal fade" style="padding-top: ; color: black;" id="formLongsor" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="">
      <div class="modal-header" >
        <h4 class="modal-title"> Form Bencana Longsor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="material-icons">clear</i>
        </button>
      </div>
      <form action="controllers/create/create_Longsor.php" method="POST">
        <div class="row">
          <div class="modal-body">
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">person</i>
                    </span>
                  </div>
                  <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                  <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                  <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">

                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                  <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">           
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                  <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                  <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">    
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">supervisor_account</i>
                    </span>
                  </div>
                  <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                  <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">date_range</i>
                    </span>
                  </div>          
                  <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" required>
                </div>
              </div>
            </div>     
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">attach_money</i>
                    </span>
                  </div>
                  <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                  <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-md">Simpan</button>
          <button type="reset" class="btn btn-danger" >Reset</button>
        </div>          
      </form>
    </div>
  </div>
</div>

<!-- modals gempa bumi --> 
<div class="modal fade" style="padding-top: ; color: black;" id="formGempa" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="">
      <div class="modal-header" >
        <h4 class="modal-title"> Form Bencana Gempa Bumi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="material-icons">clear</i>
        </button>
      </div>
      <form action="controllers/create/create_GempaBumi.php" method="POST">
        <div class="row">
          <div class="modal-body">
           <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">

           <!-- <input type="hidden" style="color: black;" name="id_gempa" class="form-control" placeholder="Id Gempa Bumi" value="111"> -->
           <div class="col-lg-12 col-sm-12">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">person</i>
                  </span>
                </div>
                <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" required>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">

              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">assignment</i>
                  </span>
                </div>
                <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">           
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">assignment</i>
                  </span>
                </div>
                <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" required>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">assignment</i>
                  </span>
                </div>
                <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" required>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">    
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">supervisor_account</i>
                  </span>
                </div>
                <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" required>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">date_range</i>
                  </span>
                </div>          
                <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" required>
              </div>
            </div>
          </div>     
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">attach_money</i>
                  </span>
                </div>
                <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-md">Simpan</button>
        <button type="reset" class="btn btn-danger" >Reset</button>
      </div>          
    </form>
  </div>
</div>
</div>


<!-- modals kebakaran --> 
<div class="modal fade" style="padding-top: ; color: black;" id="formKebakaran" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="">
      <div class="modal-header" >
        <h4 class="modal-title"> Form Bencana Kebakaran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="material-icons">clear</i>
        </button>
      </div>
      <form action="controllers/create/create_Kebakaran.php" method="POST">
        <div class="row">
          <div class="modal-body">
            <!-- <input type="hidden" style="color: black;" name="id_kebakaran" class="form-control" placeholder="Id Gempa Bumi" value="111"> -->
            <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">

            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">person</i>
                    </span>
                  </div>
                  <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                  <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">

                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                  <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">           
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                  <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                  <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">    
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">supervisor_account</i>
                    </span>
                  </div>
                  <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                  <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">date_range</i>
                    </span>
                  </div>          
                  <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" required>
                </div>
              </div>
            </div>     
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">attach_money</i>
                    </span>
                  </div>
                  <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                  <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-md">Simpan</button>
          <button type="reset" class="btn btn-danger" >Reset</button>
        </div>          
      </form>
    </div>
  </div>
</div>

<!-- modals Banjir --> 
<div class="modal fade" style="padding-top: ; color: black;" id="formBanjir" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="">
      <div class="modal-header" >
        <h4 class="modal-title"> Form Bencana Banjir</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="material-icons">clear</i>
        </button>
      </div>
      <form action="controllers/create/create_Banjir.php" method="POST">
        <div class="row">
          <div class="modal-body">
            <!-- <input type="hidden" style="color: black;" name="id_banjir" class="form-control" placeholder="Id Banjir" value="111"> -->
            <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">

            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">person</i>
                    </span>
                  </div>
                  <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                  <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">

                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                  <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">           
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                  <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                  <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">    
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">supervisor_account</i>
                    </span>
                  </div>
                  <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                  <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">date_range</i>
                    </span>
                  </div>          
                  <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" required>
                </div>
              </div>
            </div>     
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">attach_money</i>
                    </span>
                  </div>
                  <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                  <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-md">Simpan</button>
          <button type="reset" class="btn btn-danger" >Reset</button>
        </div>          
      </form>
    </div>
  </div>
</div>
<!-- end modals form -->

<div class="modal fade" style="padding-top: ; color: black;" id="formPutingBeliung" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="">
      <div class="modal-header" >
        <h4 class="modal-title"> Form Bencana Puting Beliung</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="material-icons">clear</i>
        </button>
      </div>
      <form action="" method="POST">
        <div class="row">
          <div class="modal-body">
            <!-- <input type="hidden" style="color: black;" name="id_banjir" class="form-control" placeholder="Id Banjir" value="111"> -->
            <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
            
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">person</i>
                    </span>
                  </div>
                  <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                  <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
               
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                  <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">           
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                  <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">assignment</i>
                    </span>
                  </div>
                  <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                  <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">    
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">supervisor_account</i>
                    </span>
                  </div>
                  <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                  <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" required>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">date_range</i>
                    </span>
                  </div>          
                  <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" required>
                </div>
              </div>
            </div>     
            <div class="col-lg-12 col-sm-12">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">attach_money</i>
                    </span>
                  </div>
                  <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                  <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-md">Simpan</button>
          <button type="reset" class="btn btn-danger" >Reset</button>
        </div>          
      </form>
    </div>
  </div>
</div>

<!-- Modals -->

<div class="row" id="">
  <div class="col-md-12">
    <div class="title">
    </div>
    <div class="row">
      <div class="col-xl-3 col-lg-2">
        <div class="card card-chart">
          <div class="card-header card-header-info">
            <center>  <img src="res/img/longsor.png" width="125" height="100"></center>

          </div>
          <div class="card-body">
            <h4 class="card-title">Pendataan Longsor</h4>
            <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#formLongsor">
              <i class="material-icons">library_books</i> Masukan Data 
            </button>
          </div>
          <div class="card-footer">
            <div class="stats">

            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-lg-2">
        <div class="card card-chart">
          <div class="card-header card-header-info">
            <center>  <img src="res/img/gempa.png" width="125" height="100"></center>
          </div>
          <div class="card-body">
            <h4 class="card-title">Pendataan Gempa Bumi</h4>
            <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#formGempa">
              <i class="material-icons">library_books</i> Masukan Data
            </button>
          </div>
          <div class="card-footer">
            <div class="stats">

            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-2">
        <div class="card card-chart">
          <div class="card-header card-header-info">
           <center>  <img src="res/img/kebakaran.jpg" width="125" height="100"></center>
         </div>
         <div class="card-body">
          <h4 class="card-title">Pendataan Kebakaran</h4>
          <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#formKebakaran">
            <i class="material-icons">library_books</i> Masukan Data
          </button>
        </div>
        <div class="card-footer">
          <div class="stats">

          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-lg-2">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <center>  <img src="res/img/banjir.png" width="125" height="100"></center>
        </div>
        <div class="card-body">
          <h4 class="card-title">Pendataan Banjir</h4>
          <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#formBanjir">
            <i class="material-icons">library_books</i> Masukan Data
          </button>
        </div>
        <div class="card-footer">
          <div class="stats">

          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-lg-2">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <center>  <img src="res/img/puting_beliung.png" width="125" height="100"></center>
        </div>
        <div class="card-body">
          <h4 class="card-title">Pendataan Puting Beliung</h4>
          <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#formPutingBeliung">
            <i class="material-icons">library_books</i> Masukan Data
          </button>
        </div>
        <div class="card-footer">
          <div class="stats">

          </div>
        </div>
      </div>
    </div>
  </div>


</div>


</div>
<?php if(isset($_SESSION['berhasil'])): ?>
 <div class="alert alert-success">
  <div class="container">
    <div class="alert-icon">
      <i class="material-icons">info</i>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
    </button>
    <b>Info alert :</b><?php print($_SESSION['berhasil']); ?>
  </div>
</div>
<?php unset($_SESSION['berhasil']); ?>
<?php endif; ?>
<?php if(isset($_SESSION['gagal'])): ?>
 <div class="alert alert-danger">
  <div class="container">
    <div class="alert-icon">
      <i class="material-icons">info</i>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
    </button>
    <b>Info alert :</b><?php print($_SESSION['gagal']); ?>
  </div>
</div>
<?php unset($_SESSION['gagal']); ?>
<?php endif; ?>
