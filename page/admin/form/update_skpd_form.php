<?php if($page=="update_skpd_longsor") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Longsor : <?php print($_GET['id_longsor']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <h3>Apakah anda ingin memberi bantuan?</h3>
                  <form method="POST" action="controllers/update/update_SKPD/update_skpd_longsor.php">
                    <input type="hidden" name="id_longsor" value="<?= $_GET['id_longsor'] ?>">
                    <input type="hidden" name="skpd" value="Sudah Di Bantu">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_longsor" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>

<?php if($page=="update_skpd_gempaBumi") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Gempa Bumi : <?php print($_GET['id_gempabumi']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <h3>Apakah anda ingin memberi bantuan?</h3>
                  <form method="POST" action="controllers/update/update_SKPD/update_skpd_gempaBumi.php">
                    <input type="hidden" name="id_gempabumi" value="<?= $_GET['id_gempabumi'] ?>">
                    <input type="hidden" name="skpd" value="Sudah Di Bantu">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_gempaBumi" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>


<?php if($page=="update_skpd_kebakaran") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Kebakaran : <?php print($_GET['id_kebakaran']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <h3>Apakah anda ingin memberi bantuan?</h3>
                  <form method="POST" action="controllers/update/update_SKPD/update_skpd_kebakaran.php">
                    <input type="hidden" name="id_kebakaran" value="<?= $_GET['id_kebakaran'] ?>">
                    <input type="hidden" name="skpd" value="Sudah Di Bantu">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_kebakaran" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>


<?php if($page=="update_skpd_banjir") : ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Banjir : <?php print($_GET['id_banjir']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                  <h3>Apakah anda ingin memberi bantuan?</h3> 
                  <form method="POST" action="controllers/update/update_SKPD/update_skpd_banjir.php">
                    <input type="hidden" name="id_banjir" value="<?= $_GET['id_banjir'] ?>">
                    <input type="hidden" name="skpd" value="Sudah Di Bantu">
                    <button type="submit" class="btn btn-info">Simpan</button>
                    <a href="index.php?page=tabel_banjir" class="btn btn-info">Kembali</a>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
<?php endif; ?>




