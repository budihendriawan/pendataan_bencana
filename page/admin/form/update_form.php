<?php if($page=="update_longsor") : ?>
  <?php 
  include 'class/Longsor.php';
  $longsor = new Longsor();

  $id_longsor = $_GET['id_longsor'];
  $data = $longsor->getDetail($id_longsor);

  ?>

  
  <div class="content">
    <div class="container-fluid" >
      <div class="" style="padding-left: ;">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <div class="row">
                  <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                    <h3 class="card-title">Form Update Longsor <?= $_GET['id_longsor'] ?></h3>
                  </div>
                  <div class="col-md-2">      
                   <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                 </div>
               </div>

             </div>
             <div class="card-body">
              <div class="">
                <form action="controllers/update/update_Longsor.php" method="POST" enctype="multipart/form-data">
                  <div class="row">
                    <div class="">
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">person</i>
                              </span>
                            </div>
                            <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                            <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 10%;">Nama Lengkap</label>
                            <input type="hidden" name="id_longsor" class="form-control" value="<?= $_GET['id_longsor'] ?>" id="nama lengkap" autocomplete="off" required>               
                            <input type="text" name="nama_lengkap" class="form-control" value="<?= $data['nama_lengkap'] ?>" id="nama lengkap" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">person</i>
                              </span>
                            </div>
                            <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                            <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 10%;">Umur</label>
                            <input type="number" name="umur" class="form-control" value="<?= $data['umur'] ?>" id="nama lengkap" autocomplete="off" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">

                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">assignment</i>
                              </span>
                            </div>
                            <label for="kecamatan" class="bmd-label-floating" style="padding-left: 10%;">Kecamatan</label>
                            <input type="text" name="kecamatan" class="form-control" value="<?= $data['kecamatan'] ?>" id="kecamatan" autocomplete="off" required>
                          </div>

                        </div>
                      </div>
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">           
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">assignment</i>
                              </span>
                            </div>
                            <label for="kelurahan" class="bmd-label-floating" style="padding-left: 10%;">Kelurahan</label>
                            <input type="text" name="kelurahan" class="form-control" id="kelurahan" value="<?= $data['kelurahan'] ?>" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">assignment</i>
                              </span>
                            </div>
                            <label for="dusun" class="bmd-label-floating" style="padding-left: 10%;">Dusun</label>
                            <input type="text" name="dusun" class="form-control" value="<?= $data['dusun'] ?>" id="dusun" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">    
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">supervisor_account</i>
                              </span>
                            </div>
                            <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 10%;">Jumlah Jiwa</label>
                            <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" value="<?= $data['jumlah_jiwa'] ?>" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">date_range</i>
                              </span>
                            </div>          
                            <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" value="<?= $data['tanggal_terjadi'] ?>" placeholder="" required>
                          </div>
                        </div>
                      </div>     
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">attach_money</i>
                              </span>
                            </div>
                            <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 10%;">Taksiran Kerugian</label>
                            <input type="number" name="taksiran_kerugian" class="form-control" value="<?= $data['taksiran_kerugian'] ?>" id="taksiran_kerugian" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">assignment</i>
                              </span>
                            </div>
                            <label for="dusun" class="bmd-label-floating" style="padding-left: 10%;">Sebab Kejadian</label>
                            <textarea type="text" name="sebab_kejadian" class="form-control" id="dusun" autocomplete="off" required><?= $data['sebab_kejadian'] ?></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12 col-sm-12">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="material-icons">assignment</i>
                              </span>
                            </div>
                            <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 10%;">Tindakan yang di lakukan</label>
                            <input type="text" name="tindakan_dilakukan" class="form-control" id="" value="<?= $data['tindakan_dilakukan'] ?>" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="row"> 
                        <div class="col-md-6 col-sm-10">
                          <input type="file" name="gambar1" class="btn btn-primary" title="Foto ke 1">
                        </div>
                        <div class="col-md-6 col-sm-10">
                          <input type="file" name="gambar2" class="btn btn-primary" title="Foto ke 2">
                        </div>
                      </div>
                      <div class="row"> 
                        <div class="col-md-6 col-sm-10">
                          <input type="file" name="gambar3" class="btn btn-primary" title="Foto ke 3">
                        </div>
                        <div class="col-md-6 col-sm-10">
                          <input type="file" name="gambar4" class="btn btn-primary" title="Foto ke 4">
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                    <button type="reset" class="btn btn-danger" >Reset</button>
                    <a href="index.php?page=tabel_longsor" class="btn btn-info" >Kembali</a>

                  </div>          
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php endif; ?>




<?php if($page=="update_gempaBumi") : ?>
  <?php 
  include 'class/Gempa_Bumi.php';
  $gempaBumi = new Gempa_Bumi();

  $id_gempabumi = $_GET['id_gempabumi'];
  $data = $gempaBumi->getDetail($id_gempabumi);

  ?>

  
  <div class="content">
    <div class="container-fluid" >
      <div class="" style="padding-left: 25%;">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <div class="row">
                  <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                   <h3 class="card-title">Id Gempa : <?php print($_GET['id_gempabumi']); ?></h3>
                 </div>
                 <div class="col-md-2">      
                   <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                 </div>
               </div>

             </div>
             <div class="card-body">
              <div class="">
               <form action="controllers/update/update_gempaBumi.php" method="POST">
                <div class="row">

                  <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">person</i>
                          </span>
                        </div>
                        <input type="hidden" name="id_gempabumi" value="<?= $_GET['id_gempabumi'] ?>">
                        <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                        <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                        <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" value="<?= $data['nama_lengkap'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">

                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                        <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" value="<?= $data['kecamatan'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">           
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                        <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" value="<?= $data['kelurahan'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                        <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" value="<?= $data['dusun'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">    
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">supervisor_account</i>
                          </span>
                        </div>
                        <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                        <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" value="<?= $data['jumlah_jiwa'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">date_range</i>
                          </span>
                        </div>          
                        <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" value="<?= $data['tanggal_terjadi'] ?>">
                      </div>
                    </div>
                  </div>     
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">attach_money</i>
                          </span>
                        </div>
                        <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                        <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" value="<?= $data['taksiran_kerugian'] ?>">
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-lg-12" align="right">
                  <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                  <button type="reset" class="btn btn-danger" >Reset</button>
                  <a href="index.php?page=tabel_gempaBumi" class="btn btn-info" >Kembali</a>


                </div>


              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php endif; ?>


<?php if($page=="update_kebakaran") : ?>
  <?php 
  include 'class/Kebakaran.php';
  $kebakaran = new Kebakaran();

  $id_kebakaran = $_GET['id_kebakaran'];
  $data = $kebakaran->getDetail($id_kebakaran);

  ?>

  
  <div class="content">
    <div class="container-fluid" >
      <div class="" style="padding-left: 25%;">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <div class="row">
                  <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                   <h3 class="card-title">Id Kebakaran : <?php print($_GET['id_kebakaran']); ?></h3>
                 </div>
                 <div class="col-md-2">      
                   <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                 </div>
               </div>

             </div>
             <div class="card-body">
              <div class="">
               <form action="controllers/update/update_kebakaran.php" method="POST">
                <div class="row">

                  <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">person</i>
                          </span>
                        </div>
                        <input type="hidden" name="id_kebakaran" value="<?= $_GET['id_kebakaran'] ?>">
                        <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                        <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                        <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" value="<?= $data['nama_lengkap'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">

                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                        <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" value="<?= $data['kecamatan'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">           
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                        <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" value="<?= $data['kelurahan'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                        <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" value="<?= $data['dusun'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">    
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">supervisor_account</i>
                          </span>
                        </div>
                        <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                        <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" value="<?= $data['jumlah_jiwa'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">date_range</i>
                          </span>
                        </div>          
                        <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" value="<?= $data['tanggal_terjadi'] ?>">
                      </div>
                    </div>
                  </div>     
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">attach_money</i>
                          </span>
                        </div>
                        <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                        <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" value="<?= $data['taksiran_kerugian'] ?>">
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-lg-12" align="right">
                  <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                  <button type="reset" class="btn btn-danger" >Reset</button>
                  <a href="index.php?page=tabel_kebakaran" class="btn btn-info" >Kembali</a>


                </div>


              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php endif; ?>



<?php if($page=="update_banjir") : ?>
  <?php 
  include 'class/Banjir.php';
  $banjir = new Banjir();

  $id_banjir = $_GET['id_banjir'];
  $data = $banjir->getDetail($id_banjir);

  ?>

  
  <div class="content">
    <div class="container-fluid" >
      <div class="" style="padding-left: 25%;">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <div class="row">
                  <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                   <h3 class="card-title">Id Banjir : <?php print($_GET['id_banjir']); ?></h3>
                 </div>
                 <div class="col-md-2">      
                   <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                 </div>
               </div>

             </div>
             <div class="card-body">
              <div class="">
               <form action="controllers/update/update_banjir.php" method="POST">
                <div class="row">

                  <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">person</i>
                          </span>
                        </div>
                        <input type="hidden" name="id_banjir" value="<?= $_GET['id_banjir'] ?>">
                        <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                        <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 12%;">Nama Lengkap</label>
                        <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" value="<?= $data['nama_lengkap'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">

                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="kecamatan" class="bmd-label-floating" style="padding-left: 12%;">Kecamatan</label>
                        <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" value="<?= $data['kecamatan'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">           
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="kelurahan" class="bmd-label-floating" style="padding-left: 12%;">Kelurahan</label>
                        <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" value="<?= $data['kelurahan'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">assignment</i>
                          </span>
                        </div>
                        <label for="dusun" class="bmd-label-floating" style="padding-left: 12%;">Dusun</label>
                        <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" value="<?= $data['dusun'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">    
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">supervisor_account</i>
                          </span>
                        </div>
                        <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 12%;">Jumlah Jiwa</label>
                        <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" value="<?= $data['jumlah_jiwa'] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">date_range</i>
                          </span>
                        </div>          
                        <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" value="<?= $data['tanggal_terjadi'] ?>">
                      </div>
                    </div>
                  </div>     
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">attach_money</i>
                          </span>
                        </div>
                        <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 12%;">Taksiran Kerugian</label>
                        <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" value="<?= $data['taksiran_kerugian'] ?>">
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-lg-12" align="right">
                  <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                  <button type="reset" class="btn btn-danger" >Reset</button>
                  <a href="index.php?page=tabel_banjir" class="btn btn-info" >Kembali</a>


                </div>


              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php endif; ?>




<?php if($page=="ubah_admin") : ?>
  <?php 
  include 'class/Admin.php';
  $admin = new Admin();

  $id_admin = $_SESSION['berhasil_masuk']['id_admin'];
  $data = $admin->getDetail($id_admin);

  ?>

  
  <div class="content">
    <div class="container-fluid" >
      <div class="" style="padding-left: 25%;">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <div class="row">
                  <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                   <h3 class="card-title">Id Admin : <?php print($id_admin); ?></h3>
                 </div>
                 <div class="col-md-2">      
                   <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
                 </div>
               </div>

             </div>
             <div class="card-body">
              <div class="">
               <form action="controllers/update/update_Admin.php" method="POST">
                <div class="row">



                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">person</i>
                          </span>
                        </div>
                        <input type="hidden" name="id_admin" value="<?php print($id_admin); ?>">
                        <label for="nama_admin" class="bmd-label-floating" style="padding-left: 12%;">Nama Admin</label>
                        <input type="text" name="nama_admin" class="form-control" id="nama_admin" autocomplete="off" value="" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">face</i>
                          </span>
                        </div>
                        <label for="username" class="bmd-label-floating" style="padding-left: 12%;">Username Admin</label>
                        <input type="text" name="username" class="form-control" id="username" autocomplete="off" value="" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">lock</i>
                          </span>
                        </div>
                        <label for="password" class="bmd-label-floating" style="padding-left: 12%;">Password Admin</label>
                        <input type="password" name="password" class="form-control" id="password" autocomplete="off" value="" required>
                      </div>
                    </div>
                  </div>
                  


                </div>
                <div class="col-lg-12" align="right">
                  <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                  <button type="reset" class="btn btn-danger" >Reset</button>
                  <a href="index.php" class="btn btn-info" >Kembali</a>


                </div>


              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php endif; ?>