<?php if(isset($_SESSION['berhasil'])): ?>
 <div class="alert alert-success">
  <div class="container">
    <div class="alert-icon">
      <i class="material-icons">info</i>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
    </button>
    <b>Info alert :</b><?php print($_SESSION['berhasil']); ?>
  </div>
</div>
<?php unset($_SESSION['berhasil']); ?>
<?php endif; ?>
<?php if(isset($_SESSION['gagal'])): ?>
 <div class="alert alert-danger">
  <div class="container">
    <div class="alert-icon">
      <i class="material-icons">info</i>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
    </button>
    <b>Info alert :</b><?php print($_SESSION['gagal']); ?>
  </div>
</div>
<?php unset($_SESSION['gagal']); ?>
<?php endif; ?>


<div class="content">
  <div class="container-fluid" >
    <div class="" style="padding-left: ;">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header card-header-primary">
              <div class="row">
                <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                 <h3 class="card-title">Form Pendataan Bencana</h3>
               </div>
               <div class="col-md-2">      
                 <!-- <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a> -->
               </div>
             </div>

           </div>
           <div class="card-body">
            <div class="">
              <form action="controllers/create/create.php" method="POST" enctype="multipart/form-data">
                <div class="row">
                  <div class="">
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">assignment</i>
                            </span>
                          </div>
                          <!-- <label for="kecamatan" class="bmd-label-floating" style="padding-left: 10%;">Bencana Alam</label> -->
                          <select class="form-control " name="bencana_alam" data-style="btn btn-primary btn-round" style="padding-left: 1%;">
                            <option value="">-- Pilih Bencana --</option>
                            <option value="2">Longsor</option>
                            <option value="3">Gempa Bumi</option>
                            <option value="4">Kebakaran</option>
                            <option value="5">Banjir</option>
                            <option value="6">Angin Puting Beliung</option>


                          </select>
                          <!-- <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">person</i>
                            </span>
                          </div>
                          <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                          <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 10%;">Nama Lengkap</label>
                          <input type="text" name="nama_lengkap" class="form-control" id="nama lengkap" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">person</i>
                            </span>
                          </div>
                          <input type="hidden" name="admin_penginput" value="<?php print($_SESSION['berhasil_masuk']['nama_admin']); ?>">
                          <label for="nama lengkap" class="bmd-label-floating" style="padding-left: 10%;">Umur</label>
                          <input type="number" name="umur" class="form-control" id="nama lengkap" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">assignment</i>
                            </span>
                          </div>
                          <label for="kecamatan" class="bmd-label-floating" style="padding-left: 10%;">Kecamatan</label>
                          <input type="text" name="kecamatan" class="form-control" id="kecamatan" autocomplete="off" required>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">           
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">assignment</i>
                            </span>
                          </div>
                          <label for="kelurahan" class="bmd-label-floating" style="padding-left: 10%;">Kelurahan</label>
                          <input type="text" name="kelurahan" class="form-control" id="kelurahan" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">assignment</i>
                            </span>
                          </div>
                          <label for="dusun" class="bmd-label-floating" style="padding-left: 10%;">Dusun</label>
                          <input type="text" name="dusun" class="form-control" id="dusun" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">    
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">supervisor_account</i>
                            </span>
                          </div>
                          <label for="jumlah_jiwa" class="bmd-label-floating" style="padding-left: 10%;">Jumlah Jiwa</label>
                          <input type="number" name="jumlah_jiwa" class="form-control" id="jumlah_jiwa" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">date_range</i>
                            </span>
                          </div>          
                          <input type="date" style="color: black;" name="tanggal_kejadian" class="form-control" placeholder="" required>
                        </div>
                      </div>
                    </div>     
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">attach_money</i>
                            </span>
                          </div>
                          <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 10%;">Taksiran Kerugian</label>
                          <input type="number" name="taksiran_kerugian" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">assignment</i>
                            </span>
                          </div>
                          <label for="dusun" class="bmd-label-floating" style="padding-left: 10%;">Sebab Kejadian</label>
                          <textarea type="text" name="sebab_kejadian" class="form-control" id="dusun" autocomplete="off" required></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-12 col-sm-12">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">assignment</i>
                            </span>
                          </div>
                          <label for="taksiran_kerugian" class="bmd-label-floating" style="padding-left: 10%;">Tindakan yang di lakukan</label>
                          <input type="text" name="tindakan_dilakukan" class="form-control" id="taksiran_kerugian" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="col-md-6 col-sm-10">
                        <input type="file" name="gambar1" class="btn btn-primary" title="Foto ke 1">
                      </div>
                      <div class="col-md-6 col-sm-10">
                        <input type="file" name="gambar2" class="btn btn-primary" title="Foto ke 2">
                      </div>
                    </div>
                    <div class="row"> 
                      <div class="col-md-6 col-sm-10">
                        <input type="file" name="gambar3" class="btn btn-primary" title="Foto ke 3">
                      </div>
                      <div class="col-md-6 col-sm-10">
                        <input type="file" name="gambar4" class="btn btn-primary" title="Foto ke 4">
                      </div>
                    </div>

                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary btn-md">Simpan</button>
                  <button type="reset" class="btn btn-danger" >Reset</button>
                </div>          
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
