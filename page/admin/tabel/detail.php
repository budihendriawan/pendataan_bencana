 <?php if($page=="detail_longsor") : ?>
 <?php 
  include "class/Longsor.php";
  $longsor = new Longsor();

  $id_longsor = $_GET['id_longsor'];
  $data = $longsor->getDetail($id_longsor);

?>

  <?php if($data) : ?>
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Longsor : <?php print($_GET['id_longsor']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a>
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-hover table-white">

                      <tr border="10">
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                      </tr>
                      <tr>
                        <th class="text-right"> Nama Lengkap :</th>
                        <td class="text-left"> <?= $data['nama_lengkap'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kelurahan :</th>
                        <td class="text-left"> <?= $data['kelurahan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Dusun :</th>
                        <td class="text-left"> <?= $data['dusun'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Jumlah Jiwa :</th>
                        <td class="text-left"> <?= $data['jumlah_jiwa'] ?> Orang </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Tanggal Terjadi :</th>
                        <td class="text-left"> <?= $data['tanggal_terjadi'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Taksiran Kerugian :</th>
                        <td class="text-left"> <?= $data['taksiran_kerugian'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Kerusakan :</th>
                        <td class="text-left"> <?= $data['kerusakan'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> SKPD Pemberi :</th>
                        <td class="text-left"> <?= $data['skpd'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Admin Penginput :</th>
                        <td class="text-left"> <?= $data['admin_penginput'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Tanggal di input :</th>
                        <td class="text-left"> <?= $data['tanggal_input'] ?> </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
  <?php endif; ?>



<?php endif; ?>

<?php if($page=="detail_gempaBumi") : ?>
 <?php 
  include "class/Gempa_Bumi.php";
  $gempaBumi = new Gempa_Bumi();

  $id_gempabumi = $_GET['id_gempabumi'];
  $data = $gempaBumi->getDetail($id_gempabumi);

?>

  <?php if($data) : ?>
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Gempa Bumi : <?php print($_GET['id_gempabumi']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a>
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-hover table-white">

                      <tr border="10">
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                      </tr>
                      <tr>
                        <th class="text-right"> Nama Lengkap :</th>
                        <td class="text-left"> <?= $data['nama_lengkap'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kelurahan :</th>
                        <td class="text-left"> <?= $data['kelurahan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Dusun :</th>
                        <td class="text-left"> <?= $data['dusun'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Jumlah Jiwa :</th>
                        <td class="text-left"> <?= $data['jumlah_jiwa'] ?> Orang </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Tanggal Terjadi :</th>
                        <td class="text-left"> <?= $data['tanggal_terjadi'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Taksiran Kerugian :</th>
                        <td class="text-left"> <?= $data['taksiran_kerugian'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Kerusakan :</th>
                        <td class="text-left"> <?= $data['kerusakan'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> SKPD :</th>
                        <td class="text-left"> <?= $data['skpd'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Admin Penginput :</th>
                        <td class="text-left"> <?= $data['admin_penginput'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Tanggal di input :</th>
                        <td class="text-left"> <?= $data['tanggal_input'] ?> </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
  <?php endif; ?>



<?php endif; ?>


<?php if($page=="detail_kebakaran") : ?>
 <?php 
  include "class/Kebakaran.php";
  $kebakaran = new Kebakaran();

  $id_kebakaran = $_GET['id_kebakaran'];
  $data = $kebakaran->getDetail($id_kebakaran);

?>

  <?php if($data) : ?>
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Kebakaran : <?php print($_GET['id_kebakaran']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <a href="index.php?page=tabel_kebakaran" class="btn btn-white"><span style="color: black;"> Kembali</span></a>
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-hover table-white">

                      <tr border="10">
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                      </tr>
                      <tr>
                        <th class="text-right"> Nama Lengkap :</th>
                        <td class="text-left"> <?= $data['nama_lengkap'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kelurahan :</th>
                        <td class="text-left"> <?= $data['kelurahan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Dusun :</th>
                        <td class="text-left"> <?= $data['dusun'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Jumlah Jiwa :</th>
                        <td class="text-left"> <?= $data['jumlah_jiwa'] ?> Orang </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Tanggal Terjadi :</th>
                        <td class="text-left"> <?= $data['tanggal_terjadi'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Taksiran Kerugian :</th>
                        <td class="text-left"> <?= $data['taksiran_kerugian'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Kerusakan :</th>
                        <td class="text-left"> <?= $data['kerusakan'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> SKPD :</th>
                        <td class="text-left"> <?= $data['skpd'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Admin Penginput :</th>
                        <td class="text-left"> <?= $data['admin_penginput'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Tanggal di input :</th>
                        <td class="text-left"> <?= $data['tanggal_input'] ?> </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
  <?php endif; ?>



<?php endif; ?>


<?php if($page=="detail_banjir") : ?>
 <?php 
  include "class/Banjir.php";
  $banjir = new Banjir();

  $id_banjir = $_GET['id_banjir'];
  $data = $banjir->getDetail($id_banjir);

?>

  <?php if($data) : ?>
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-10" style="padding: 10px 10px 10px 10px;">
                       <h3 class="card-title">Id Banjir : <?php print($_GET['id_banjir']); ?></h3>
                    </div>
                    <div class="col-md-2">      
                       <a href="index.php?page=tabel_longsor" class="btn btn-white"><span style="color: black;"> Kembali</span></a>
                    </div>
                </div>
                 
                </div>
                   <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-hover table-white">

                      <tr border="10">
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                      </tr>
                      <tr>
                        <th class="text-right"> Nama Lengkap :</th>
                        <td class="text-left"> <?= $data['nama_lengkap'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kelurahan :</th>
                        <td class="text-left"> <?= $data['kelurahan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Kecamatan :</th>
                        <td class="text-left"> <?= $data['kecamatan'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Dusun :</th>
                        <td class="text-left"> <?= $data['dusun'] ?> </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Jumlah Jiwa :</th>
                        <td class="text-left"> <?= $data['jumlah_jiwa'] ?> Orang </td>
                      </tr>
                       <tr>
                        <th class="text-right"> Tanggal Terjadi :</th>
                        <td class="text-left"> <?= $data['tanggal_terjadi'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Taksiran Kerugian :</th>
                        <td class="text-left"> <?= $data['taksiran_kerugian'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Kerusakan :</th>
                        <td class="text-left"> <?= $data['kerusakan'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> SKPD Pemberi :</th>
                        <td class="text-left"> <?= $data['skpd'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Admin Penginput :</th>
                        <td class="text-left"> <?= $data['admin_penginput'] ?> </td>
                      </tr>
                      <tr>
                        <th class="text-right"> Tanggal di input :</th>
                        <td class="text-left"> <?= $data['tanggal_input'] ?> </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
  <?php endif; ?>
<?php endif; ?>