<?php 

require_once "class/Longsor.php";
$longsor = new Longsor(); 
$jumlah_longsor = $longsor->getJumlahData_longsor();

require_once "class/Kebakaran.php";
$kebakaran = new Kebakaran(); 
$jumlah_kebakaran = $kebakaran->getJumlahData_kebakaran();

require_once "class/Gempa_Bumi.php";
$gempaBumi = new Gempa_Bumi(); 
$jumlah_gempa = $gempaBumi->getJumlahData_gempaBumi();

require_once "class/Banjir.php";
$banjir = new Banjir(); 
$jumlah_banjir = $banjir->getJumlahData_banjir();

require_once "class/Puting_beliung.php";
$puting_beliung = new Puting_Beliung(); 
$jumlah_putingBeliung = $puting_beliung->getJumlahData_putingBeliung();


?>



<div class="row" id="">
  <div class="col-md-12">
    <div class="title">
    </div>
    <div class="row">
      <div class="col-xl-3 col-lg-2">
        <div class="card card-chart">
          <div class="card-header card-header-info">
            <h4>Jumlah Data : <?= $jumlah_longsor['0']; ?></h4>
          </div>
          <div class="card-body">
            <h4 class="card-title">Tabel Longsor</h4>
            <a href="index.php?page=tabel_longsor" class="btn btn-block btn-primary" >
              <i class="material-icons">visibility</i>&nbsp; Lihat Tabel
            </a>
          </div>
          <div class="card-footer">
            <div class="stats">
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-2">
        <div class="card card-chart">
          <div class="card-header card-header-info">
            <h4>Jumlah Data :  <?= $jumlah_gempa['0']; ?></h4>
          </div>
          <div class="card-body">
            <h4 class="card-title">Tabel Gempa Bumi</h4>
            <a href="index.php?page=tabel_gempaBumi" class="btn btn-block btn-primary" >
              <i class="material-icons">visibility</i>&nbsp; Lihat Tabel      
            </a>
          </div>
          <div class="card-footer">
            <div class="stats">                               
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-2">
        <div class="card card-chart">
          <div class="card-header card-header-info">
           <h4>Jumlah Data :   <?= $jumlah_kebakaran['0']; ?></h4>
         </div>
         <div class="card-body">
          <h4 class="card-title">Tabel Kebakaran</h4>
          <a href="index.php?page=tabel_kebakaran" class="btn btn-block btn-primary" >
            <i class="material-icons">visibility</i>&nbsp; Lihat Tabel               
          </a>
        </div>
        <div class="card-footer">
          <div class="stats">                              
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-2">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <h4>Jumlah Data : <?= $jumlah_banjir['0']; ?></h4>
        </div>
        <div class="card-body">
          <h4 class="card-title">Tabel Banjir</h4>
          <a href="index.php?page=tabel_banjir" class="btn btn-block btn-primary" >
            <i class="material-icons">visibility</i>&nbsp; Lihat Tabel                  
          </a>
        </div>
        <div class="card-footer">
          <div class="stats">    
          </div>
        </div>
      </div>
    </div>
     <div class="col-xl-3 col-lg-2">
      <div class="card card-chart">
        <div class="card-header card-header-info">
          <h4>Jumlah Data : <?= $jumlah_putingBeliung['0']; ?></h4>
        </div>
        <div class="card-body">
          <h4 class="card-title">Tabel Puting Beliung</h4>
          <a href="index.php?page=tabel_putingBeliung" class="btn btn-block btn-primary" >
            <i class="material-icons">visibility</i>&nbsp; Lihat Tabel               
          </a>
        </div>
        <div class="card-footer">
          <div class="stats">    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>         
</div>