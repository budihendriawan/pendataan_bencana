<?php
include "class/Puting_Beliung.php";
$puting_beliung = new Puting_Beliung();


?>
<!-- <script src="page/admin/ajax/ajax.js"></script> -->
<?php if(isset($_SESSION['gagal_print'])): ?>
    <div class="alert alert-danger">
        <div class="container">
            <div class="alert-icon">
                <i class="material-icons">info</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            <b>Info alert Puting Beliung : </b><?php print($_SESSION['gagal_print']); ?>
        </div>
    </div>
    <?php unset($_SESSION['gagal_print']); ?>
<?php endif; ?>
<div class="col-md-15">
    <div class="card card-chart">
        <div class="card-header card-header-info">
            <div class="row">
                <div class="col-sm-4">
                    <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#pertanggal">
                        <i class="material-icons">print</i>&nbsp; Print Per tanggal
                    </button>
                </div>
                <div class="col-sm-4">
                    <a href="" class="btn btn-block btn-primary "><i class="material-icons">print</i>&nbsp; Print Laporan</a>
                </div>
            </div>


            <!-- 
            <a href="" class="btn btn-primary"><i class="material-icons" >print</i>&nbsp; Print</a>
            <a href="" class="btn btn-primary"><i class="material-icons" >print</i>&nbsp; Print (Tahun)</a>
            <a href="" class="btn btn-primary"><i class="material-icons" >print</i>&nbsp; Print Skpd</a> -->

        </div>
        <div class="card-body">
            <h4 class="card-title">Tabel Puting Beliung</h4>
            <div class="table-responsive">
                <table id="tabel_longsor" class="align-items-center " width="1000">
                    <thead class="thead-dark">
                        <tr>
                            <th style=""><center>No &nbsp; &nbsp;</center></th>
                            <th style="">Nama Lengkap &nbsp; &nbsp; </th>
                            <th style="">Kecamatan &nbsp; &nbsp;</th>
                            <th style="">Kelurahan &nbsp; &nbsp;</th>
                            <!-- <th style="">Dusun &nbsp; &nbsp;</th> -->
                            <th style=""><center>Jumlah Jiwa &nbsp; &nbsp;</center></th>
                            <th style=""><center>Tanggal Terjadi &nbsp; &nbsp;</center></th>
                            <th style=""><center>Kerusakan &nbsp; &nbsp;</center></th>
                            <th style=""><center>Skpd &nbsp; &nbsp;</center></th>

                            <!-- <th style=""><center>Taksiran Kerugian &nbsp; &nbsp;</center></th> -->
                            <th style=""><center>Aksi &nbsp; &nbsp;</center></th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($puting_beliung->getData() as $no =>$data) : ?>
                            <tr>

                                <td align="center">  <?php echo ($no + 1) ?> </td>
                                <td align="center"><?= $data['nama_lengkap'] ?></td>
                                <td align="center"><?= $data['kecamatan'] ?></td>
                                <td align="center"><?= $data['kelurahan'] ?></td>
                                <!-- <td align="center"><?= $data['dusun'] ?></td> -->

                                <td align="center"><?= $data['jumlah_jiwa'] ?></td>
                                <td align="center"><?= $data['tanggal_terjadi'] ?></td>
                                <td align="center"><?= $data['kerusakan'] ?></td>
                                <td align="center"> <?= $data['skpd'] ?>

                                </td>

                                <!-- <td align="center"><?= $data['taksiran_kerugian'] ?></td> -->
                                <td align="center">
                                    <a href="index.php?page=detail_longsor&id_longsor=<?php echo $data['id_longsor']; ?>" class="btn btn-info btn-sm" title="Detail"><i class="material-icons" >info</i> </a>
                                    <a href="index.php?page=update_longsor&id_longsor=<?php echo $data['id_longsor']; ?>" class="btn btn-success btn-sm" title="Update"><i class="material-icons" >cached</i> </a>
                                    <a href="index.php?page=delete_putingBeliung&id_putingBeliung=<?php echo $data['id_putingBeliung']; ?>" class="btn btn-danger btn-sm" title="Delete"><i class="material-icons" >delete</i> </a>
                                    <?php if($data['skpd'] == "Belum") { ?>
                                    <a href="index.php?page=update_skpd_longsor&id_longsor=<?php echo $data['id_longsor']; ?>" class="btn btn-primary btn-sm" title="Update skpd"><i class="material-icons" >update</i> </a>
                                    <?php } ?>
                                    
                                </td>
                            </tr>
                        <?php endforeach ?> 
                    </tbody>

                </table>                
            </div>

        </div>
        <div class="card-footer">

        </div>
    </div>
</div>

<div class="modal fade" style="padding-top: ; color: black;" id="pertanggal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="">
            <div class="modal-header" >
                <h4 class="modal-title"> Form Tanggal Bencana Longsor</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <form action="" method="GET">
                <div class="row">
                    <div class="modal-body">
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>          
                                    <input type="date" style="color: black;" name="tanggalnya" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>     

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-md">Print</button>
                    <button type="reset" class="btn btn-danger" >Reset</button>
                </div>          
            </form>
        </div>
    </div>
</div>


                                    